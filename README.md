mini_admin (Boilerplate example)
===========

A Symfony project created on August 9, 2017, 3:46 pm.

#Version 1.0.0

The [first commit](https://bitbucket.org/tourpourtous/mini_admin/commits/aa92e9e62fa7ba228f710838a32d21a1c180af96) contains a boilerplate with the following extra boundles:
 
 * [jms/i18n-routing-bundle](http://jmsyst.com/bundles/JMSI18nRoutingBundle)
 * [stof/doctrine-extensions-bundle](https://github.com/Atlantic18/DoctrineExtensions)
 * [friendsofsymfony/user-bundle](http://symfony.com/doc/master/bundles/FOSUserBundle/index.html)

Has implemented translations using *jms/i18n-routing-bundle* and *stof/doctrine-extensions-bundle*, 
defining the locale via url param with fallback to default locale in content and excluding the default
locale from url (/admin instead of /es/admin in case the es is the default locale).
The user management and the user forms (login, register, profile, etc) are implemented using the most basic bundle configuration.

Is not used static texts translation nor assets translation. Either is not used other functionality from doctrine extensions than Translation (translatable).

#Version 1.1.0

Added Blameable (createdBy and updatedBy using User entity) and Timestampable (createdAt, updatedAt) features to Entities (Destination, Place) from stof_doctrine_extensions bundle.

#Version 1.2.0

Added translation to static text content like labels and menu names. Used _yml_ files for these translations.
Evaluate the use of [lexik](https://github.com/lexik/LexikTranslationBundle/blob/master/Resources/doc/index.md) to manage translation via web and export and import translations.

#Version 1.2.1

Has implemented REST apis

#Version 1.2.2

Implemented [serializers](https://symfony.com/doc/current/serializer.html)

#Version 1.2.3

Implemented [service](https://symfony.com/doc/current/service_container.html) with param configuration in service.yml file

#Version 1.3.0

Implemented form data [validation](https://symfony.com/doc/current/validation.html) (see route **admin_destination_new**) and validation message translation.
Implemented form security in edit, delete and create forms with [csrf token](https://symfony.com/doc/current/security/csrf_in_login_form.html#csrf-login-template).

#Version 1.4.0

Implemented [error pages](https://symfony.com/doc/current/controller/error_pages.html)

#Version 1.5.0

Implemented [console commands](http://symfony.com/doc/current/console.html) to import users from a csv file.

#Version 2.0.0

Implemented [doctrine filter](http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/filters.html) to exclude admin users from users list when the authenticated user is not SUPER_ADMIN.

#Version 2.1.0

Added [bootstrap toogle button](http://www.bootstraptoggle.com/)
Added [twig function](https://symfony.com/doc/current/templating/twig_extension.html)