<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-04
 * Time: 19:53
 */

namespace ContentBundle\Service;


use AppBundle\Entity\EntityTraits\TControllerAwareService;
use AppBundle\Repository\AuthorRepository;
use AppBundle\Repository\StateRepository;
use ContentBundle\Entity\Destination;
use ContentBundle\Repository\DestinationRepository;


class DestinationService
{
    use TControllerAwareService;

    public function save(DestinationRepository $repository, AuthorRepository $author_repository, StateRepository $state_repository, Destination $entity, $token="")
    {
        $locales = $this->getParameter("locales");
        $default_locale = $this->getParameter("locale");

        $state_fk = $entity->getState();

        $request = $this->getCurrentRequest();
        try{
            if($request->getMethod() == 'POST')
            {
                $submittedToken = $request->request->get("_csrf_token");
                if (!$this->isCsrfTokenValid($token, $submittedToken))
                {
                    $this->addFlash(
                        'error',
                        'message.form.invalid'
                    );

                    return $this->redirectToRoute('admin_destination_list');
                }

                $translations = $request->request->get("translations");

                $author = trim($translations[$default_locale]["author"]);
                $name = trim($translations[$default_locale]["name"]);
                $keywords = trim($translations[$default_locale]["keywords"]);
                $description = trim($translations[$default_locale]["description"]);
                $note = isset($translations[$default_locale]["note"]) ? trim($translations[$default_locale]["note"]) : null;

                $state_id = $request->request->get("state");
                $state = null;
                if ($state_id)
                {
                    $state = $state_repository->find($state_id);
                }

                $entity->setPublished($request->request->get("publish"));
                $entity->setName($name);
                $entity->setKeywords($keywords);
                $entity->setState($state);
                $entity->setDescription($description);
                $entity->setNote($note);
                $entity->setLocale($default_locale);
                $entity->setCoordinates(["lng" => $request->request->get("lng"), "lat" => $request->request->get("lat")]);

                if($author)
                {
                    $entity->setAuthor($author_repository->findOrCreate($author), $default_locale);
                }
                else
                {
                    $repository->deleteAuthor($entity, $default_locale);
                }

                // Assert: The values in the default language cannot be null or empty
                if (count($this->validate($entity))){
                    $this->addFlash(
                        'error',
                        'message.form.empty'
                    );

                    if ($token == "edit_destination")
                    {
                        return $this->render('ContentBundle:admin/destination:edit.html.twig', [
                            "token" => $token,
                            "entity" => $entity,
                            "state_id" => $state_fk ? $state_fk->getId() : Null,
                            "form_data" => [
                                "coordinates" => $entity->getCoordinates(),
                                "states" => $state_repository->findAllByCountryCode("CU"),
                                "translations" => $translations,
                                "locales" => $locales,
                                "default_locale" => $default_locale]]);
                    }
                    else
                    {
                        return $this->render('ContentBundle:admin/destination:new.html.twig', [
                            "token" => $token,
                            "form_data" => [
                                "states" => $state_repository->findAllByCountryCode("CU"),
                                "locales" => $locales,
                                "default_locale" => $default_locale
                            ]]);
                    }
                }

                unset($translations[$default_locale]);
                foreach ($translations as $locale=>$translation)
                {
                    foreach ($translation as $field=>$_value)
                    {
                        if (!in_array($locale, $locales))
                        {
                            continue;
                        }

                        $value = trim($_value);
                        if ($value)
                        {
                            if ($field == "author")
                            {
                                $entity->setAuthor($author_repository->findOrCreate($value), $locale);
                            }
                            elseif ($field == "keywords")
                            {
                                $repository->translate($entity, $field, $locale, serialize(array_map('trim', explode(',', $value))));
                            }
                            else
                            {
                                $repository->translate($entity, $field, $locale, $value);
                            }
                        }
                        elseif($token == 'edit_destination')
                        {
                            if ($field == "author")
                            {
                                $repository->deleteAuthor($entity, $locale);
                            }
                            else
                            {
                                $repository->deleteTranslation($entity, $field, $locale);
                            }
                        }
                    }
                }

                $repository->save($entity);

                $this->addFlash(
                    'success',
                    'message.save.success'
                );

                return $this->redirectToRoute('admin_destination_show', ["id"=> $entity->getId()]);
            }
        }catch (\Exception $exc){
            $this->addFlash(
                'error',
                'message.edit.error'
            );
        }

        if ($token == "edit_destination") {
            $translations = $repository->versions($entity, True);
            return $this->render('ContentBundle:admin/destination:edit.html.twig', [
                    "token" => $token,
                    "entity" => $entity,
                    "state_id" => $state_fk ? $state_fk->getId() : Null,
                    "form_data" => [
                        "coordinates" => $entity->getCoordinates(),
                        "states" => $state_repository->findAllByCountryCode("CU"),
                        "translations" => $translations,
                        "locales" => $locales,
                        "default_locale" => $default_locale]]
            );
        }
        else
        {
            return $this->render('ContentBundle:admin/destination:new.html.twig', [
                "token" => $token,
                "form_data" => [
                    "states" => $state_repository->findAllByCountryCode("CU"),
                    "locales" => $locales,
                    "default_locale" => $default_locale
                ]]);
        }
    }
}
