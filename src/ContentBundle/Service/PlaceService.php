<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-04
 * Time: 19:53
 */

namespace ContentBundle\Service;


use AppBundle\Entity\EntityTraits\TControllerAwareService;
use AppBundle\Repository\AuthorRepository;
use AppBundle\Repository\TranslationRepository;
use ContentBundle\Entity\Place;
use ContentBundle\Repository\ContentRepository;
use ContentBundle\Repository\DestinationRepository;


class PlaceService
{
    use TControllerAwareService;

    public function save(ContentRepository $repository,
                         DestinationRepository $destinationRepository,
                         AuthorRepository $author_repository,
                         TranslationRepository $categoryRepository,
                         Place $entity,
                         $token="")
    {
        $locales = $this->getParameter("locales");
        $default_locale = $this->getParameter("locale");
        $destination_versioned = $destinationRepository->getAllVersioned();
        $destination_fk = $entity->getDestination();
        $category_fk = $entity->getCategory();

        $request = $this->getCurrentRequest();
        try{
            if($request->getMethod() == 'POST'){

                $submittedToken = $request->request->get("_csrf_token");
                if (!$this->isCsrfTokenValid($token, $submittedToken)) {
                    $this->addFlash(
                        'error',
                        'message.form.invalid'
                    );

                    return $this->redirectToRoute('admin_place_list');
                }

                $translations = $request->request->get("translations");

                $author = trim($translations[$default_locale]["author"]);
                $name = trim($translations[$default_locale]["name"]);
                $description = trim($translations[$default_locale]["description"]);
                $note = trim($translations[$default_locale]["note"]);
                $destination_param = $request->request->get("destination");
                $category_id = $request->request->get("category");

                $destination = null;
                if ($destination_param)
                {
                    $destination = $destinationRepository->find($destination_param);
                }

                $category = null;
                if ($category_id)
                {
                    $category = $categoryRepository->find($category_id);
                }

                $entity->setPublished($request->request->get("publish"));
                $entity->setName($name);
                $entity->setNote($note);
                $entity->setDescription($description);
                $entity->setDestination($destination);
                $entity->setCategory($category);
                $entity->setLocale($default_locale);
                $entity->setCoordinates(["lng" => $request->request->get("lng"), "lat" => $request->request->get("lat")]);

                if($author)
                {
                    $entity->setAuthor($author_repository->findOrCreate($author), $default_locale);
                }
                else
                {
                    $repository->deleteAuthor($entity, $default_locale);
                }

                // Assert: The values in the default language cannot be null or empty
                if (count($this->validate($entity))){
                    $this->addFlash(
                        'error',
                        'message.form.empty'
                    );

                    if ($token == "edit_place")
                    {
                        return $this->render('ContentBundle:admin/place:edit.html.twig', [
                            "token" => $token,
                            "entity" => $entity,
                            "destination_id" => $destination_fk ? $destination_fk->getId() : Null,
                            "category_id" => $category_fk ? $category_fk->getId() : Null,
                            "form_data" => [
                                "coordinates" => $entity->getCoordinates(),
                                "translations" => $translations,
                                "destinations" => $destination_versioned,
                                "categories" => $categoryRepository->getAllVersioned(),
                                "locales" => $locales,
                                "default_locale" => $default_locale]]);
                    }
                    else
                    {
                        return $this->render('ContentBundle:admin/place:new.html.twig', [
                            "token" => $token,
                            "form_data" => [
                                "destinations" => $destination_versioned,
                                "categories" => $categoryRepository->getAllVersioned(),
                                "locales" => $locales,
                                "default_locale" => $default_locale
                            ]]);
                    }
                }

                unset($translations[$default_locale]);
                foreach ($translations as $locale=>$translation)
                {
                    foreach ($translation as $field=>$_value)
                    {
                        if (!in_array($locale, $locales))
                        {
                            continue;
                        }

                        $value = trim($_value);
                        if ($value)
                        {
                            if ($field == "author")
                            {
                                $entity->setAuthor($author_repository->findOrCreate($value), $locale);
                            }
                            else
                            {
                                $repository->translate($entity, $field, $locale, $value);
                            }
                        }
                        else
                        {
                            if ($field == "author")
                            {
                                $repository->deleteAuthor($entity, $locale);
                            }
                            else
                            {
                                $repository->deleteTranslation($entity, $field, $locale);
                            }
                        }
                    }
                }

                $repository->save($entity);

                $this->addFlash(
                    'success',
                    'message.save.success'
                );

                return $this->redirectToRoute('admin_place_show', ["id"=> $entity->getId()]);
            }
        }catch (\Exception $exc){
            $this->addFlash(
                'error',
                'message.edit.error'
            );
        }

        if ($token == 'edit_place')
        {
            $translations = $repository->versions($entity, True);
            return $this->render('ContentBundle:admin/place:edit.html.twig', [
                    "token" => $token,
                    "entity" => $entity,
                    "destination_id" => $destination_fk ? $destination_fk->getId() : Null,
                    "category_id" => $category_fk ? $category_fk->getId() : Null,
                    "form_data" => [
                        "coordinates" => $entity->getCoordinates(),
                        "translations" => $translations,
                        "destinations" => $destination_versioned,
                        "categories" => $categoryRepository->getAllVersioned(),
                        "locales" => $locales,
                        "default_locale" => $default_locale]]
            );
        }
        else
        {
            return $this->render('ContentBundle:admin/place:new.html.twig', [
                "token" => $token,
                "form_data" => [
                    "destinations" => $destination_versioned,
                    "categories" => $categoryRepository->getAllVersioned(),
                    "locales" => $locales,
                    "default_locale" => $default_locale
                ]]);
        }
    }
}
