<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-18
 * Time: 15:58
 */

namespace ContentBundle\Repository;


use AppBundle\Entity\Author;
use AppBundle\Entity\State;
use ContentBundle\Entity\ContentEntity;
use AppBundle\Repository\TranslationRepository;
use ContentBundle\Entity\Destination;
use ContentBundle\Entity\PlaceCategory;


class ContentRepository extends TranslationRepository
{
    protected function getIgnoredAttributes(): array
    {
        return array_merge(parent::getIgnoredAttributes(), ['lng',
            'lat',
            "authors",
            "coordinates"]);
    }

    public function deleteAuthor(ContentEntity $entity, $language)
    {
        $relation = $entity->unsetAuthor($language);

        if ($relation)
        {
            $class = get_class($relation);
            return $this->_em->createQueryBuilder()
                ->delete($class, 'a')
                ->where('a.id = :id')
                ->getQuery()
                ->setParameter('id', $relation->getId())
                ->execute();
        }

        return null;
    }

    protected function getNormalizerCallbacks($locale)
    {
        $author_callback = function ($author) {
            return $author instanceof Author
                ? $author->getName()
                : '';
        };

        $destination_callback = function ($destination) use ($locale){
            if ($destination instanceof Destination){
                $destination->setLocale($locale);
                $this->_em->refresh($destination);

                return ["id" =>$destination->getId(), "name" => $destination->getName()];
            }
            return "";
        };
        $category_callback = function ($category) use ($locale){
            if ($category instanceof PlaceCategory){
                $category->setLocale($locale);
                $this->_em->refresh($category);

                return ["id" =>$category->getId(), "name" => $category->getName()];
            }
            return "";
        };

        $state_callback = function ($state) use ($locale){
            if ($state instanceof State){
                return ["id" =>$state->getId(), "name" => $state->getName()];
            }
            return "";
        };

        return [
            'author' => $author_callback,
            "destination" => $destination_callback,
            "category" => $category_callback,
            "state" => $state_callback
        ];
    }

    protected function processVersionTranslation($rawTranslation, $normalizedTranslation)
    {
        if (!empty($normalizedTranslation['author']))
            $rawTranslation["author"] = $normalizedTranslation["author"];

//                if (!empty($translation['destination']))
//                    $versions[$locale]["destination"] = $normalized["destination"];

        if (!empty($rawTranslation['keywords']))
            $rawTranslation["keywords"] = $normalizedTranslation["keywords"];

        return $rawTranslation;
    }
}
