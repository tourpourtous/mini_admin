<?php
/**
 * Created by Ernesto Baez.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-09
 * Time: 19:46
 */

namespace ContentBundle\Repository;


class DestinationRepository extends ContentRepository
{
    protected function getIgnoredAttributes(): array
    {
        return array_merge(parent::getIgnoredAttributes(), [
            "authors",
            "coordinates"
        ]);
    }

    public function findAllOrderedByName()
    {
        return $this->createQueryBuilder('d')
            ->orderBy('d.name', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
