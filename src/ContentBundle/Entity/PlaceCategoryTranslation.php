<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-10
 * Time: 19:34
 */

namespace ContentBundle\Entity;


use AppBundle\Entity\BaseTranslation;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="place_category_translations",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="place_cat_tr_unique_idx", columns={
 *         "locale", "object_id", "field"
 *     })}
 * )
 */
class PlaceCategoryTranslation extends BaseTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="PlaceCategory", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $object;
}
