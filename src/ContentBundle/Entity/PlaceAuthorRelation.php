<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-17
 * Time: 15:13
 */

namespace ContentBundle\Entity;

use AppBundle\Entity\ContentAuthor;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="place_author",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="place_author_unique_idx", columns={
 *         "locale", "author", "place"
 *     })})
 */
class PlaceAuthorRelation extends ContentAuthor
{
    /**
     * @ORM\ManyToOne(targetEntity="Place", cascade={"persist"}, inversedBy="authors")
     * @ORM\JoinColumn(name="place", referencedColumnName="id")
     */
    protected $content;
}
