<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-10
 * Time: 19:34
 */

namespace ContentBundle\Entity;


use AppBundle\Entity\BaseTranslation;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="destination_translations",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="destination_tr_unique_idx", columns={
 *         "locale", "object_id", "field"
 *     })})
 */
class DestinationTranslation extends BaseTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="Destination", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $object;
}
