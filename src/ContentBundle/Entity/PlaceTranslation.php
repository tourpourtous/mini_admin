<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-10
 * Time: 19:34
 */

namespace ContentBundle\Entity;


use AppBundle\Entity\BaseTranslation;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="place_translations",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="place_tr_unique_idx", columns={
 *         "locale", "object_id", "field"
 *     })}
 * )
 */
class PlaceTranslation extends BaseTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="Place", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $object;
}
