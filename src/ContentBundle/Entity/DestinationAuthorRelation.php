<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-17
 * Time: 15:13
 */

namespace ContentBundle\Entity;

use AppBundle\Entity\ContentAuthor;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="destination_author",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="destination_author_unique_idx", columns={
 *         "locale", "author", "destination"
 *     })})
 */
class DestinationAuthorRelation extends ContentAuthor
{
    /**
     * @ORM\ManyToOne(targetEntity="Destination", inversedBy="authors")
     * @ORM\JoinColumn(name="destination", referencedColumnName="id")
     */
    protected $content;
}
