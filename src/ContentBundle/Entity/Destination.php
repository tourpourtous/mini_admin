<?php
/**
 * Created by Ernesto Baez.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-09
 * Time: 19:01
 */

namespace ContentBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="destination")
 * @Gedmo\TranslationEntity(class="DestinationTranslation")
 * @ORM\Entity(repositoryClass="ContentBundle\Repository\DestinationRepository")
 */
class Destination extends ContentEntity
{
    /**
     * @ORM\OneToMany(
     *   targetEntity="DestinationTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * @ORM\OneToMany(targetEntity="DestinationAuthorRelation", mappedBy="content", cascade={"persist", "remove"})
     */
    protected $authors;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\State")
     * @ORM\JoinColumn(name="state_id", referencedColumnName="id")
     */
    private $state;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="array", nullable=true)
     */
    private $keywords;

    public function setState($value)
    {
        $this->state = $value;
    }

    public function getState()
    {
        return $this->state;
    }

    public function getKeywordsArray()
    {
        try{
            if (is_string($this->keywords))
            {
                $this->keywords = unserialize($this->keywords);
            }
            if (is_array($this->keywords))
            {
                return $this->keywords;
            }
        }catch (\Exception $exc){}

        return [];
    }

    public function getKeywords()
    {
        $keywords = $this->getKeywordsArray();
        return $keywords ? implode(", ", $keywords) : "";
    }

    /**
     * @param mixed $value
     */
    public function setKeywords($value)
    {
        if(is_array($value))
        {
            $this->keywords = $value;
        }else{
            $this->keywords = array_map('trim', explode(',', $value));
        }
    }

    protected function getContentAuthorInstance($author, $language)
    {
        return new DestinationAuthorRelation($this, $author, $language);
    }
}
