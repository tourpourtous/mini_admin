<?php
/**
 * Created by Ernesto Baez.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-09
 * Time: 19:01
 */

namespace ContentBundle\Entity;


use AppBundle\Entity\EntityTraits\TCategory;
use AppBundle\Entity\EntityTraits\TDestination;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="place")
 * @Gedmo\TranslationEntity(class="PlaceTranslation")
 */
class Place extends ContentEntity
{
    use TCategory;
    use TDestination;

    /**
     * @ORM\OneToMany(
     *   targetEntity="PlaceTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;

    /**
     * @ORM\ManyToOne(targetEntity="PlaceCategory")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;

    /**
     * @ORM\OneToMany(targetEntity="PlaceAuthorRelation", mappedBy="content", cascade={"persist", "remove"})
     */
    protected $authors;

    protected function getContentAuthorInstance($author, $language)
    {
        return new PlaceAuthorRelation($this, $author, $language);
    }
}
