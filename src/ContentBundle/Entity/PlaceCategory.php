<?php
/**
 * Created by Ernesto Baez.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-09
 * Time: 15:08
 */

namespace ContentBundle\Entity;


use AppBundle\Entity\Category;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Asserts;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Entity
 * @ORM\Table(name="place_category")
 * @Gedmo\TranslationEntity(class="PlaceCategoryTranslation")
 * @UniqueEntity("name")
 */
class PlaceCategory extends Category
{
    /**
     * @ORM\OneToMany(
     *   targetEntity="PlaceCategoryTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;
}
