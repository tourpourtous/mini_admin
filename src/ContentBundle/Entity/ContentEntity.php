<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-17
 * Time: 10:36
 */

namespace ContentBundle\Entity;


use AppBundle\Entity\BaseEntity;
use AppBundle\Entity\EntityStatus;
use AppBundle\Entity\EntityTraits\TAuthor;
use AppBundle\Entity\EntityTraits\TLocation;
use AppBundle\Entity\EntityTraits\TPublish;
use AppBundle\Entity\EntityTraits\TTranslatableDescription;
use AppBundle\Entity\EntityTraits\TTranslatableName;
use AppBundle\Entity\EntityTraits\TTranslatableNote;
use AppBundle\Entity\EntityTraits\TTranslate;
use AppBundle\Entity\ITranslate;
use AppBundle\Repository\VariableRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Asserts;


/**
 * Class ContentEntity
 * @package AppBundle\Entity
 * @ORM\MappedSuperclass(repositoryClass="ContentBundle\Repository\ContentRepository")
 * @UniqueEntity("name")
 */
abstract class ContentEntity extends BaseEntity implements ITranslate
{
    use TAuthor;
    use TTranslatableName;
    use TTranslatableDescription;
    use TTranslatableNote;
    use TLocation;
    use TPublish;
    use TTranslate {
        __construct as private initialize;
        calculateStatus as protected _calculateStatus;
    }

    public function __construct()
    {
        $this->initialize();
        $this->authors = new ArrayCollection();

        $this->status = [];
    }

    protected function calculateStatus()
    {
        $this->_calculateStatus(function(&$status)
        {
            $default_locale = VariableRepository::getDefaultLocale();

            if ((method_exists($this, 'getState') && !$this->getState()) ||
                (method_exists($this, 'getCategory') && !$this->getCategory()) ||
                (method_exists($this, 'getKeywords') && !$this->getKeywords()) ||
                (method_exists($this, 'getDestination') && !$this->getDestination()) ||
                ($this->hasDefaultCoordinates()))
            {
                $status[$default_locale] = EntityStatus::ES_STATUS_WARNING;
            }
        });
    }

    public function getTranslatableFields()
    {
        return ["name", "description", "note"];
    }
}
