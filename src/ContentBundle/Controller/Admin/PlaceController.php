<?php
/**
 * Created by Ernesto Baez.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-09
 * Time: 15:02
 */

namespace ContentBundle\Controller\Admin;


use AppBundle\Entity\Author;
use ContentBundle\Entity\Destination;
use ContentBundle\Entity\Place;
use ContentBundle\Entity\PlaceCategory;
use ContentBundle\Service\PlaceService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PlaceController extends Controller
{
    /**
     * @Route("/place/", name="admin_place_list")
     */
    public function listAction()
    {
        $items = $this->getDoctrine()
            ->getRepository(Place::class)
            ->findAll();

        // replace this example code with whatever you need
        return $this->render('ContentBundle:admin/place:list.html.twig', ["items" => $items]);
    }

    /**
     * @Route("/place/{id}/", name="admin_place_show", requirements={"id" = "\d+"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id)
    {
        $repository = $this->getDoctrine()
            ->getRepository(Place::class);

        $entity = $repository
            ->find($id);

        if (!$entity) {
            $this->addFlash("warn", "message.get.error");
            return $this->redirectToRoute('admin_place_list');
        }

        $translations = $repository->versions($entity);

        return $this->render('ContentBundle:admin/place:details.html.twig',
            [
                "entity"=> $entity,
                "form_data" => [
                    "prev" => $repository->findPrev($entity),
                    "next" => $repository->findNext($entity),
                    "locales" => $this->container->getParameter("locales"),
                    "translations" => $translations,
                    "default_locale" => $this->container->getParameter("locale")
                ]
            ]
        );
    }

    /**
     * @Route("/place/new/", name="admin_place_new")
     * @Method({"GET", "POST"})
     * @param PlaceService $placeService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(PlaceService $placeService)
    {
        $repository = $this->getDoctrine()
            ->getRepository(Place::class);

        $author_repository = $this->getDoctrine()
            ->getRepository(Author::class);

        $destination_repository = $this->getDoctrine()
            ->getRepository(Destination::class);

        $category_repository = $this->getDoctrine()
            ->getRepository(PlaceCategory::class);

        $entity = new Place();

        return $placeService->save($repository, $destination_repository, $author_repository, $category_repository, $entity, 'new_place');
    }

    /**
     * @Route("/place/{id}/edit/", name="admin_place_edit", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     * @param $id
     * @param PlaceService $placeService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id, PlaceService $placeService)
    {
        $repository = $this->getDoctrine()
            ->getRepository(Place::class);

        $entity = $repository->find($id);

        if (!$entity) {
            $this->addFlash("warn", "message.get.error");
            return $this->redirectToRoute('admin_place_list');
        }

        $destination_repository = $this->getDoctrine()
            ->getRepository(Destination::class);

        $author_repository = $this->getDoctrine()
            ->getRepository(Author::class);

        $category_repository = $this->getDoctrine()
            ->getRepository(PlaceCategory::class);

        return $placeService->save($repository, $destination_repository, $author_repository, $category_repository, $entity, 'edit_place');
    }

    /**
     * @Route("/place/{id}/delete/", name="admin_place_delete", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($id, Request $request)
    {
        $repository = $this->getDoctrine()
            ->getRepository(Place::class);

        $entity = $repository
            ->find($id);

        if (!$entity) {
            $this->addFlash("warn", "message.get.error");
            return $this->redirectToRoute('admin_place_list');
        }

        try{

            if($request->getMethod() == 'POST'){

                $submittedToken = $request->request->get("_csrf_token");
                if (!$this->isCsrfTokenValid('delete_place', $submittedToken)) {
                    $this->addFlash(
                        'success',
                        'message.form.invalid'
                    );

                    return $this->redirectToRoute('admin_place_list');
                }

                $repository->delete($entity);

                $this->addFlash(
                    'success',
                    'message.delete.success'
                );

                return $this->redirectToRoute('admin_place_list');
            }
        }catch (\Exception $exc){
            $this->addFlash(
                'error',
                'message.delete.error'
            );
        }

        $translations = $repository->versions($entity);

        return $this->render('ContentBundle:admin/place:delete.html.twig', [
            "entity" => $entity,
            "form_data" => [
                "default_locale" => $this->container->getParameter("locale"),
                "locales" => $this->container->getParameter("locales"),
                "translations" => $translations
            ]
        ]);
    }

}
