<?php
/**
 * Created by Ernesto Baez.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-09
 * Time: 15:02
 */

namespace ContentBundle\Controller\Admin;


use AppBundle\Entity\Author;
use AppBundle\Entity\State;
use ContentBundle\Entity\Destination;
use ContentBundle\Service\DestinationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class DestinationController extends Controller
{
    /**
     * @Route("/destination/", name="admin_destination_list")
     */
    public function listAction()
    {
        $items = $this->getDoctrine()
            ->getRepository(Destination::class)
            ->findAll();

        return $this->render('ContentBundle:admin/destination:list.html.twig', ["items" => $items]);
    }

    /**
     * @Route("/destination/{id}/", name="admin_destination_show", requirements={"id" = "\d+"})
     */
    public function showAction($id)
    {
        $repository = $this->getDoctrine()
            ->getRepository(Destination::class);

        $entity = $repository
            ->find($id);

        $entity->isOk();

        if (!$entity) {
            $this->addFlash("warn", "message.get.error");
            return $this->redirectToRoute('admin_destination_list');
        }

        $translations = $repository->versions($entity);

        return $this->render('ContentBundle:admin/destination:details.html.twig',
            [
                "entity"=> $entity,
                "form_data" => [
                    "prev" => $repository->findPrev($entity),
                    "next" => $repository->findNext($entity),
                    "locales" => $this->container->getParameter("locales"),
                    "translations" => $translations
                ]
            ]
        );
    }

    /**
     * @Route("/destination/new/", name="admin_destination_new")
     * @Method({"GET", "POST"})
     */
    public function createAction(DestinationService $destinationService)
    {
        $token = 'new_destination';

        $repository = $this->getDoctrine()
            ->getRepository(Destination::class);

        $author_repository = $this->getDoctrine()
            ->getRepository(Author::class);

        $state_repository = $this->getDoctrine()
            ->getRepository(State::class);

        $entity = new Destination();

        return $destinationService->save($repository, $author_repository, $state_repository, $entity, $token);
    }

    /**
     * @Route("/destination/{id}/edit/", name="admin_destination_edit", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     */
    public function editAction($id, DestinationService $destinationService)
    {
        $token = 'edit_destination';

        $repository = $this->getDoctrine()
            ->getRepository(Destination::class);

        $entity = $repository
            ->find($id);

        if (!$entity) {
            $this->addFlash("warn", "message.get.error");
            return $this->redirectToRoute('admin_destination_list');
        }

        $author_repository = $this->getDoctrine()
            ->getRepository(Author::class);

        $state_repository = $this->getDoctrine()
            ->getRepository(State::class);

        return $destinationService->save($repository, $author_repository, $state_repository, $entity, $token);
    }

    /**
     * @Route("/destination/{id}/delete/", name="admin_destination_delete", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     */
    public function deleteAction($id, Request $request)
    {
        $repository = $this->getDoctrine()
            ->getRepository(Destination::class);

        $entity = $repository
            ->find($id);

        if (!$entity) {
            $this->addFlash("warn", "message.get.error");
            return $this->redirectToRoute('admin_destination_list');
        }

        try{
            if($request->getMethod() == 'POST'){
                $submittedToken = $request->request->get("_csrf_token");
                if (!$this->isCsrfTokenValid('delete_destination', $submittedToken)) {
                    $this->addFlash(
                        'error',
                        'message.form.invalid'
                    );

                    return $this->redirectToRoute('admin_destination_list');
                }

                $repository->delete($entity);

                $this->addFlash(
                    'success',
                    'message.delete.success'
                );

                return $this->redirectToRoute('admin_destination_list');
            }
        }catch (\Exception $exc){
            $this->addFlash(
                'error',
                'message.delete.error'
            );
        }

        $translations = $repository->versions($entity);

        return $this->render('ContentBundle:admin/destination:delete.html.twig', [
            "entity" => $entity,
            "form_data" => [
                "locales" => $this->container->getParameter("locales"),
                "translations" => $translations
            ]
        ]);
    }

}
