<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-04
 * Time: 12:00
 */

namespace InfoBundle\Service;


use AppBundle\Entity\EntityTraits\TControllerAwareService;
use AppBundle\Repository\TranslationRepository;
use InfoBundle\Entity\InfoPage;
use InfoBundle\Entity\InfoPageCategoryRelation;


class InfoPageService
{
    use TControllerAwareService;

    public function save(TranslationRepository $repository, InfoPage $entity, $category_repository, $token=null)
    {
        $default_locale = $this->getParameter("locale");
        $locales = $this->getParameter("locales");

        $translations = [];
        $request = $this->getCurrentRequest();
        try {
            if ($request->getMethod() == 'POST')
            {
                $submittedToken = $request->request->get("_csrf_token");
                if (!$this->isCsrfTokenValid($token, $submittedToken)) {
                    $this->addFlash(
                        'error',
                        'message.form.invalid'
                    );

                    return $this->redirectToRoute('admin_infopage_list', ["pageType" => $entity->getPageType()->getCode()]);
                }

                $translations = $request->request->get("translations");

                $name = trim($translations[$default_locale]["name"]);
                $description = trim($translations[$default_locale]["description"]);
                $note = trim($translations[$default_locale]["note"]);

                $removed_categories = [];
                if ($token == "edit_info")
                {
                    $removed_categories = $entity->getCategories();
                }

                if ($categories = $request->request->get("categories"))
                {
                    $_categories = [];
                    foreach (explode(",", $categories) as $category_id)
                    {
                        if ($category = $category_repository->find($category_id))
                        {
                            $_categories[] = $category;
                        }
                    }

                    $removed_categories = $entity->setCategories($_categories);
                }

                foreach ($removed_categories as $key => $removed_category)
                {
                    $entity->deleteCategory($key);
                    $_result = $repository->deleteCategory($entity, $removed_category);
                }

                $entity->setPublished($request->request->get("publish"));
                $entity->setName($name);
                $entity->setDescription($description);
                $entity->setNote($note);

                if (count($this->validate($entity))) {
                    $this->addFlash(
                        'error',
                        'message.form.empty'
                    );
                }
                else
                {
                    unset($translations[$default_locale]);

                    foreach ($translations as $locale=>$translation)
                    {
                        foreach ($translation as $field=>$_value)
                        {
                            if (!in_array($locale, $locales))
                            {
                                continue;
                            }

                            $value = trim($_value);
                            if ($value)
                            {
                                $repository->translate($entity, $field, $locale, $value);
                            }
                            else
                            {
                                $repository->deleteTranslation($entity, $field, $locale);
                            }
                        }
                    }

                    $repository->save($entity);

                    $this->addFlash(
                        'success',
                        'message.save.success'
                    );
                    return $this->redirectToRoute('admin_infopage_show', ["pageType" => $entity->getPageType()->getCode(), "id" => $entity->getId()]);
                }
            }
        }catch (\Exception $exc)
        {
            $this->addFlash(
                'error',
                'message.edit.error'
            );
        }

        if (!$translations && $token == "edit_info")
            $translations = $repository->versions($entity, True);

        return $this->render('InfoBundle:admin/info:input.html.twig', [
            "entity" => $entity,
            "token" => $token,
            "form_data" => [
                "categories" => $category_repository->getIdNameList(),
                "translations" => $translations]
        ]);
    }
}