<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-19
 * Time: 09:00
 */

namespace InfoBundle\Entity;


use AppBundle\Entity\BaseEntity;
use AppBundle\Entity\EntityTraits\TPublish;
use AppBundle\Entity\EntityTraits\TTranslatableDescription;
use AppBundle\Entity\EntityTraits\TTranslatableName;
use AppBundle\Entity\EntityTraits\TTranslatableNote;
use AppBundle\Entity\EntityTraits\TTranslate;
use AppBundle\Entity\ITranslate;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Asserts;


/**
 * @ORM\Table(name="info_page")
 * @ORM\Entity(repositoryClass="InfoBundle\Repository\InfoPageRepository")
 * @Gedmo\TranslationEntity(class="InfoPageTranslation")
 * @UniqueEntity("name")
 */
class InfoPage extends BaseEntity implements ITranslate
{
    use TTranslatableName;
    use TTranslatableDescription;
    use TTranslatableNote;
    use TPublish;
    use TTranslate{
        __construct as private initialize;
    }

    /**
     * @ORM\OneToMany(
     *   targetEntity="InfoPageTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;

    /**
     * @ORM\OneToMany(targetEntity="InfoPageCategoryRelation", mappedBy="infoPage", cascade={"persist", "remove"})
     */
    protected $categories;

    /**
     * @ORM\ManyToOne(targetEntity="InfoPageType")
     * @ORM\JoinColumn(name="page_type_id", referencedColumnName="id", nullable=false)
     * @Asserts\NotBlank(message = "input.not_blank")
     * @Asserts\NotNull()
     */
    private $pageType;

    public function getTranslatableFields()
    {
        return ["name", "description", "note"];
    }

    public function __construct()
    {
        $this->initialize();
        $this->categories = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getPageType()
    {
        return $this->pageType;
    }

    /**
     * @param mixed $pageType
     */
    public function setPageType($pageType)
    {
        $this->pageType = $pageType;
    }

    public function getCategories()
    {
        $categories = [];

        foreach ($this->categories as $category) {
            $categories[] = $category->getCategory();
        }

        return $categories;
    }

    public function setCategories($categories)
    {
        $existing = [];
        $to_delete = [];
        foreach ($this->categories as $key => $category)
        {
            $_to_delete = true;

            foreach ($categories as $category_in) {
                if ($category->getCategory()->getId() == $category_in->getId())
                {
                    $_to_delete = false;
                    $existing[$category->getCategory()->getId()] = true;
                    break;
                }
            }

            if ($_to_delete)
            {
                $to_delete[$key] = $category->getCategory();
            }
        }

        foreach ($categories as $category)
        {
            if(!isset($existing[$category->getId()]))
            {
                $this->categories->add(new InfoPageCategoryRelation($this, $category));
            }
        }

        return $to_delete;
    }

    public function deleteCategory($key)
    {
        $this->categories->remove($key);
    }

    public function categoryListId()
    {
        $result = [];
        if ($this->categories)
        {
            foreach ($this->categories as $category)
            {
                $result[] = $category->getCategory()->getId();
            }
        }

        return $result;
    }
}
