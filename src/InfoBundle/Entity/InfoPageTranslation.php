<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-10
 * Time: 19:34
 */

namespace InfoBundle\Entity;


use AppBundle\Entity\BaseTranslation;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="info_page_translations",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="info_page_tr_unique_idx", columns={
 *         "locale", "object_id", "field"
 *     })}
 * )
 */
class InfoPageTranslation extends BaseTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="InfoPage", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $object;
}
