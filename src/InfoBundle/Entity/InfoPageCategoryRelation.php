<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-19
 * Time: 09:00
 */

namespace InfoBundle\Entity;


use AppBundle\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Asserts;


/**
 * @ORM\Entity
 * @ORM\Table(name="info_category_relation",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="info_cat_unique_idx", columns={
 *         "info_page", "category"
 *     })})
 */
class InfoPageCategoryRelation extends BaseEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="InfoPage", cascade={"persist"})
     * @ORM\JoinColumn(name="info_page", referencedColumnName="id")
     */
    private $infoPage;

    /**
     * @ORM\ManyToOne(targetEntity="InfoPageCategory", cascade={"persist"})
     * @ORM\JoinColumn(name="category", referencedColumnName="id")
     */
    private $category;

    public function __construct($infoPage, $category)
    {
        $this->infoPage = $infoPage;
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return mixed
     */
    public function getInfoPage()
    {
        return $this->infoPage;
    }
}
