<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-19
 * Time: 09:00
 */

namespace InfoBundle\Entity;


use AppBundle\Entity\Nomenclature;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Asserts;


/**
 * @ORM\Entity
 * @ORM\Table(name="info_page_type")
 * @UniqueEntity("name")
 */
class InfoPageType extends Nomenclature
{
    const TYPE_INFO = "info";
    const TYPE_IDEA = "idea";
    const TYPE_HOW_IT_WORKS = "about-site";
    const TYPE_TO_DO = "to-do";
    const TYPE_TO_KNOW = "to-know";

    /**
     * @ORM\Column(type="boolean")
     */
    private $useCategories = false;

    public static function getFixtures()
    {
        return [
            new InfoPageType(InfoPageType::TYPE_INFO, false),
            new InfoPageType(InfoPageType::TYPE_IDEA, false),
            new InfoPageType(InfoPageType::TYPE_HOW_IT_WORKS),
            new InfoPageType(InfoPageType::TYPE_TO_DO),
            new InfoPageType(InfoPageType::TYPE_TO_KNOW)
        ];
    }

    public function __construct($code = "", $useCategory = true)
    {
        parent::__construct($code);

        $this->useCategories = $useCategory;
    }

    public function useCategories()
    {
        return $this->useCategories;
    }
}
