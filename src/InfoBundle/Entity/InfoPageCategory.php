<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-19
 * Time: 09:00
 */

namespace InfoBundle\Entity;


use AppBundle\Entity\Category;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Asserts;


/**
 * @ORM\Entity
 * @ORM\Table(name="info_page_category")
 * @Gedmo\TranslationEntity(class="InfoCategoryTranslation")
 * @UniqueEntity("name")
 */
class InfoPageCategory extends Category
{
    /**
     * @ORM\OneToMany(
     *   targetEntity="InfoCategoryTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;
}
