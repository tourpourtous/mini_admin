<?php

namespace InfoBundle\Controller\Admin;


use InfoBundle\Entity\InfoPage;
use InfoBundle\Entity\InfoPageCategory;
use InfoBundle\Entity\InfoPageType;
use InfoBundle\Service\InfoPageService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class InfoPageController extends Controller
{
    /**
     * @Route("/", name="admin_infopage_list")
     * @param $pageType
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction($pageType)
    {
        $_type = $this->getDoctrine()
            ->getRepository(InfoPageType::class)
            ->findOneBy(["code" => $pageType]);

        $entities = $this->getDoctrine()
            ->getRepository(InfoPage::class)
            ->findBy(["pageType" => $_type->getId()]);

        return $this->render('InfoBundle:admin/info:list.html.twig', ["pageType" => $pageType, "items" => $entities]);
    }

    /**
     * @Route("/{id}/", name="admin_infopage_show", requirements={"id" = "\d+"})
     * @param $pageType
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function showAction($pageType, $id)
    {
        $repository = $this->getDoctrine()
            ->getRepository(InfoPage::class);

        $entity = $repository->find($id);

        if (!$entity || $entity->getPageType()->getCode() != $pageType) {
            $this->addFlash("warn", "message.get.error");
            return $this->redirectToRoute('admin_infopage_list', ["pageType" => $pageType]);
        }

        $translations = $repository->versions($entity);

        return $this->render('InfoBundle:admin/info:details.html.twig',
            [
                "entity" => $entity,
                "form_data" => [
                    "prev" => $repository->findPrev($entity),
                    "next" => $repository->findNext($entity),
                    "translations" => $translations
                ]
            ]
        );
    }

    /**
     * @Route("/new/", name="admin_infopage_new")
     * @Method({"GET", "POST"})
     * @param $pageType
     * @param InfoPageService $infoPageService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction($pageType, InfoPageService $infoPageService)
    {
        $token = 'new_info';

        $repository = $this->getDoctrine()
            ->getRepository(InfoPage::class);

        $_type = $this->getDoctrine()
            ->getRepository(InfoPageType::class)
            ->findOneBy(["code" => $pageType]);

        $entity = new InfoPage();
        $entity->setPageType($_type);

        $categories_repository = $this->getDoctrine()
            ->getRepository(InfoPageCategory::class);

        return $infoPageService->save($repository, $entity, $categories_repository, $token);
    }

    /**
     * @Route("/{id}/edit/", name="admin_infopage_edit", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     * @param $pageType
     * @param $id
     * @param InfoPageService $infoPageService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($pageType, $id, InfoPageService $infoPageService)
    {
        $token = "edit_info";

        $repository = $this->getDoctrine()
            ->getRepository(InfoPage::class);

        $entity = $repository->find($id);

        if (!$entity || $entity->getPageType()->getCode() != $pageType) {
            $this->addFlash("warn", "message.get.error");
            return $this->redirectToRoute('admin_infopage_list', ["pageType" => $pageType]);
        }

        $categories_repository = $this->getDoctrine()
            ->getRepository(InfoPageCategory::class);

        return $infoPageService->save($repository, $entity, $categories_repository, $token);
    }

    /**
     * @Route("/{id}/delete/", name="admin_infopage_delete", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     * @param $pageType
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($pageType, $id, Request $request)
    {
        $repository = $this->getDoctrine()
            ->getRepository(InfoPage::class);

        $entity = $repository->find($id);

        if (!$entity) {
            $this->addFlash("warn", "message.get.error");
            return $this->redirectToRoute('admin_infopage_list', ["pageType" => $pageType]);
        }
        try {
            if ($request->getMethod() == 'POST') {
                $submittedToken = $request->request->get("_csrf_token");
                if (!$this->isCsrfTokenValid('delete_infopage', $submittedToken)) {
                    $this->addFlash(
                        'error',
                        'message.form.invalid'
                    );
                } else {
                    $repository->delete($entity);

                    $this->addFlash(
                        'success',
                        'message.delete.success'
                    );
                }

                return $this->redirectToRoute('admin_infopage_list', ["pageType" => $pageType]);
            }
        } catch (\Exception $exc) {
            $this->addFlash(
                'error',
                'message.delete.error'
            );
        }

        $translations = $repository->versions($entity);

        return $this->render('InfoBundle:admin/info:delete.html.twig',
            [
                "entity" => $entity,
                "form_data" => [
                    "prev" => $repository->findPrev($entity),
                    "next" => $repository->findNext($entity),
                    "translations" => $translations
                ]
            ]
        );
    }
}
