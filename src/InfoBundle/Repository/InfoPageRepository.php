<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-19
 * Time: 09:00
 */

namespace InfoBundle\Repository;


use AppBundle\Entity\IBaseEntity;
use AppBundle\Repository\TranslationRepository;
use InfoBundle\Entity\InfoPageCategoryRelation;


/**
 * InfoPageRepository
 */
class InfoPageRepository extends TranslationRepository
{
    protected function getIgnoredAttributes(): array
    {
        return array_merge(parent::getIgnoredAttributes(), ["pageType"]);
    }

    protected function getNormalizerCallbacks($locale)
    {
        return [ "categories" => function ($categories) use ($locale) {
            $result = [];

            foreach ($categories as $category)
            {
                $category->setLocale($locale);
                $this->_em->refresh($category);

                $result[] = $category->getName();
            }

            return $result;
        }];
    }

    public function findPrev(IBaseEntity $entity)
    {
        $result = $this->createQueryBuilder('d')
//            ->leftJoin('d.pageType', 'page_type')
//            ->andWhere('page_type.code like :pt')
            ->orderBy('d.id', 'DESC')
            ->where('d.id < :id')
            ->andWhere('d.pageType = :pt')
            ->setParameter('id', $entity->getId())
            ->setParameter('pt', $entity->getPageType())
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return count($result) ? $result[0] : Null;
    }

    public function findNext(IBaseEntity $entity)
    {
        $result = $this->createQueryBuilder('d')
            ->orderBy('d.id', 'ASC')
            ->where('d.id > :id')
            ->andWhere('d.pageType = :pt')
            ->setParameter('id', $entity->getId())
            ->setParameter('pt', $entity->getPageType())
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return count($result) ? $result[0] : Null;
    }

    public function deleteCategory(IBaseEntity $entity, IBaseEntity $category)
    {
        if (($entityId = $entity->getId()) && ($categoryId = $category->getId())) {

            return $this->_em->createQueryBuilder()
                ->delete(InfoPageCategoryRelation::class, 'entity_category')
                ->where('entity_category.infoPage = :entityId',
                    'entity_category.category = :categoryId')
                ->getQuery()
                ->execute(compact('entityId', 'categoryId'));
        }

        return null;
    }

}
