<?php
/**

 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-15
 * Time: 13:56
 */

namespace UserBundle\Controller\API;

use Doctrine\ORM\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UserBundle\Entity\User;

class UserApiV1 extends Controller
{
    /**
     * @param $field
     * @param $value
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/verify_uniqueness/{id}/{field}/{value}", name="api_user_verify_uniqueness")
     * @Method({"GET"})
     */
    public function verifyUniqueness($id, $field, $value)
    {
        $result = false;
        $userId = null;
        $customerNumber = null;

        if ($value)
        {
            $value = base64_decode($value);
            $entities = $this->getDoctrine()
                ->getManager()
                ->getRepository(User::class)
                ->createQueryBuilder('u')
                ->where("upper(u.$field) = upper(:value)")
                ->andWhere("u.id != :id")
                ->setParameter('value', $value)
                ->setParameter('id', $id)
                ->getQuery()
                ->getResult(Query::HYDRATE_OBJECT);

            if($result = count($entities))
            {
                $userId = $entities[0]->getId();
                $customerNumber = $entities[0]->getCustomerNumber();
            }
        }

        return $this->json(["unique" => boolval(!$result), "user_id" => $userId, "customer_number" => $customerNumber]);
    }
}
