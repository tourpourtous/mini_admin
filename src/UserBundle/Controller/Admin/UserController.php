<?php

namespace UserBundle\Controller\Admin;

use AppBundle\Entity\State;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;
use UserBundle\Service\UserService;

/**
 * Class UserController
 * @package UsersBundle\Controller\Admin
 */
class UserController extends Controller
{
    /**
     * @Route("/user/", name="admin_user_list")
     */
    public function listAction()
    {
        $items = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();

        return $this->render('UserBundle:admin/user:list.html.twig', ["items" => $items]);
    }

    /**
     * @Route("/user/{id}/", name="admin_user_show", requirements={"id" = "\d+"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id)
    {
        $repository = $this->getDoctrine()
            ->getRepository(User::class);

        $entity = $repository->find($id);

        if (!$entity) {
            $this->addFlash("warn", "message.get.error");
            return $this->redirectToRoute('admin_user_list');
        }

        return $this->render('UserBundle:admin/user:details.html.twig',[
            "entity"=> $entity,
            "form_data" => [
                "prev" => $repository->findPrev($entity),
                "next" => $repository->findNext($entity)
            ]
        ]);
    }

    /**
     * @Route("/user/new/", name="admin_user_new")
     * @Method({"GET", "POST"})
     * @param UserService $userService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(UserService $userService)
    {
        $token = 'new_user';

        $repository = $this->getDoctrine()
            ->getRepository(User::class);

        $stateRepository = $this->getDoctrine()
            ->getRepository(State::class);
        $entity = new User();

        return $userService->save($repository, $stateRepository, $entity, $token);
    }

    /**
     * @Route("/user/{id}/edit/", name="admin_user_edit", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     * @param $id
     * @param UserService $userService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id, UserService $userService)
    {
        $token = "edit_user";
        $repository = $this->getDoctrine()
            ->getRepository(User::class);

        $stateRepository = $this->getDoctrine()
            ->getRepository(State::class);

        $entity = $repository->find($id);

        if (!$entity) {
            $this->addFlash("warn", "message.get.error");
            return $this->redirectToRoute('admin_user_list');
        }

        return $userService->save($repository, $stateRepository, $entity, $token);
    }

    /**
     * @Route("/user/{id}/delete/", name="admin_user_delete", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($id, Request $request)
    {
        $repository = $this->getDoctrine()
            ->getRepository(User::class);

        $entity = $repository->find($id);

        if (!$entity) {
            $this->addFlash("warn", "message.get.error");
            return $this->redirectToRoute('admin_user_list');
        }
        try {
            if ($request->getMethod() == 'POST')
            {
                $submittedToken = $request->request->get("_csrf_token");
                if (!$this->isCsrfTokenValid('delete_user', $submittedToken)) {
                    $this->addFlash(
                        'error',
                        'message.form.invalid'
                    );
                }
                else
                {
                    $repository->delete($entity);

                    $this->addFlash(
                        'success',
                        'message.delete.success'
                    );
                }

                return $this->redirectToRoute('admin_user_list');
            }
        }catch (\Exception $exc)
        {
            $this->addFlash(
                'error',
                'message.delete.error'
            );
        }

        return $this->render('UserBundle:admin/user:delete.html.twig',["entity"=> $entity]);
    }
}
