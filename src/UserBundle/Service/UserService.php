<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-04
 * Time: 12:00
 */

namespace UserBundle\Service;


use AppBundle\Entity\Address;
use AppBundle\Entity\EntityTraits\TControllerAwareService;
use AppBundle\Repository\StateRepository;
use UserBundle\Entity\User;
use UserBundle\Repository\UserRepository;


class UserService
{
    use TControllerAwareService;

    public function save(UserRepository $repository, StateRepository $stateRepository, User $entity, $token=null)
    {
        $request = $this->getCurrentRequest();
        try {
            if ($request->getMethod() == 'POST')
            {
                $submittedToken = $request->request->get("_csrf_token");
                if (!$this->isCsrfTokenValid($token, $submittedToken)) {
                    $this->addFlash(
                        'error',
                        'message.form.invalid'
                    );

                    return $this->redirectToRoute('admin_user_list');
                }

                $entity->setUsername(trim($request->request->get("username")));
                $entity->setName($request->request->get("name"));
                $entity->setLastName($request->request->get("lastName"));
                $entity->setEmail($request->request->get("email"));
                $entity->setCell($request->request->get("cell"));
                $entity->setLandNumber($request->request->get("landNumber"));
                $entity->setCustomerNumber($request->request->get("customerNumber"));
                $entity->setNote($request->request->get("note"));

                $address_data = $request->request->get("address");
                if($address_data && trim($address_data["address1"]))
                {
                    $address = new Address();
                    $address->setAddress1($address_data["address1"]);
                    $address->setAddress2($address_data["address2"]);
                    $address->setCity($address_data["city"] ?? "");
                    $address->setState($address_data["state"]);
                    $address->setCountry("Cuba");
                    $address->setZip($address_data["zip"]);
                    $address->setCoordinates($address_data["coordinates"]);

                    $entity->setAddress($address);

                    if (count($this->validate($address)))
                    {
                        $this->addFlash(
                            'error',
                            'message.form.incomplete_address'
                        );

                        return $this->render('UserBundle:admin/user:input.html.twig', [
                            "entity" => $entity,
                            "states_cities" => $stateRepository->statesCitiesTree("CU"),
                            "token" => $token]);
                    }
                }
                else
                {
                    if ($address = $entity->getAddress())
                    {
                        //todo: verify address references and remove if not referenced anymore

                        $entity->setAddress(null);
                    }
                }

                $numbers = [$entity->getCell(), $entity->getLandNumber()];

                foreach ($numbers as $number)
                {
                    if ( $number && substr( $number, 0, 1 ) != "+")
                    {
                        $this->addFlash(
                            'error',
                            'message.form.number_format'
                        );

                        return $this->render('UserBundle:admin/user:input.html.twig', [
                            "entity" => $entity,
                            "states_cities" => $stateRepository->statesCitiesTree("CU"),
                            "token" => $token]);
                    }
                }

                if (count($this->validate($entity)) ||
                    !($entity->getCell() || $entity->getEmail()) ||
                    !($entity->getUsername() || $entity->getEmail()))
                {
                    $this->addFlash(
                        'error',
                        'message.form.wrong_values'
                    );

                    return $this->render('UserBundle:admin/user:input.html.twig', [
                        "entity" => $entity,
                        "states_cities" => $stateRepository->statesCitiesTree("CU"),
                        "token" => $token]);
                }

                $repository->save($entity);

                $this->addFlash(
                    'success',
                    'message.save.success'
                );
                return $this->redirectToRoute('admin_user_show', ["id" => $entity->getId()]);
            }
        }catch (\Exception $exc)
        {
            $this->addFlash(
                'error',
                'message.edit.error'
            );
        }

        return $this->render('UserBundle:admin/user:input.html.twig', [
            "entity" => $entity,
            "states_cities" => $stateRepository->statesCitiesTree("CU"),
            "token" => $token]);
    }
}
