<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-03
 * Time: 13:55
 */

namespace UserBundle\Repository;


use AppBundle\Entity\EntityStatus;
use AppBundle\Repository\BaseEntityRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping;
use UserBundle\Entity\User;


class UserRepository extends BaseEntityRepository
{
    public function findAll($orderBy='customerNumber', $order='ASC')
    {
        return $this->findBy([], [$orderBy => $order]);
    }
}
