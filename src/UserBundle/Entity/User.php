<?php
/**
 * Created by Ernesto Baez.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-09
 * Time: 15:08
 */

namespace UserBundle\Entity;


use AppBundle\Entity\EntityTraits\TAddress;
use AppBundle\Entity\EntityTraits\TCustomerNumber;
use AppBundle\Entity\EntityTraits\TEntityStatus;
use AppBundle\Entity\IBaseEntity;
use AppBundle\Entity\EntityStatus;
use AppBundle\Entity\EntityTraits\TDescription;
use AppBundle\Entity\EntityTraits\TName;
use AppBundle\Entity\EntityTraits\TNote;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Asserts;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
 * @ORM\AttributeOverrides({
 *     @ORM\AttributeOverride(name="username", column=@ORM\Column(unique=true)),
 *     @ORM\AttributeOverride(name="usernameCanonical", column=@ORM\Column(unique=true)),
 *     @ORM\AttributeOverride(name="email", column=@ORM\Column(nullable=true, unique=true)),
 *     @ORM\AttributeOverride(name="emailCanonical", column=@ORM\Column(nullable=true, unique=true))
 * })
 * @UniqueEntity("username")
 * @UniqueEntity("email")
 * @UniqueEntity("cellNumber")
 * @UniqueEntity("customerNumber")
 */
class User extends BaseUser implements IBaseEntity
{
    use TCustomerNumber;
    use TDescription;
    use TNote;
    use TName;
    use TAddress;
    use TEntityStatus{
        beforeSave as private _beforeSave;
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=64, options={"default": ""})
     * @Asserts\NotBlank(message = "input.not_blank")
     * @Asserts\NotNull()
     */
    protected $name="";

    /**
     * @ORM\Column(type="text", length=128, options={"default": ""})
     */
    protected $lastName = "";

    /**
     * @ORM\Column(type="text", length=20, nullable=true, unique=true)
     */
    protected $cellNumber;

    /**
     * @ORM\Column(type="text", length=20, nullable=true)
     */
    protected $landNumber;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description="";

//    /**
//     * Avatar image
//     */
//    private $avatar;

    public function getLastName(){
        return $this->lastName;
    }

    public function setLastName($value){
        $this->lastName = trim($value);
    }

    public function setEmail($email)
    {
        if (!$email)
        {
            $email = null;
        }

        return parent::setEmail($email);
    }

    public function getCell(){
        return $this->cellNumber;
    }

    public function setCell($value){
        if (!trim($value))
        {
            $value = null;
        }

        $this->cellNumber = $value;
    }

    public function getLandNumber(){
        return $this->landNumber;
    }

    public function setLandNumber($value){
        if (!trim($value))
        {
            $value = null;
        }

        $this->landNumber = trim($value);
    }

    public function beforeSave()
    {
        if (!$this->getUsername())
        {
            parent::setUsername($this->email);
        }

        if (!$this->getPassword())
        {
            $this->setPassword((new \DateTime())->getTimestamp());
        }

        $this->_beforeSave();
    }

    protected function getStatusMapping()
    {
        return ["u" => "ENTITY", "n" => "NOTE"];
    }

    protected function calculateStatus()
    {
        $status["u"] = EntityStatus::ENTITY_STATUS_SUCCESS;
        $status["n"] = EntityStatus::NOTE_STATUS_SUCCESS;

        if ($note = $this->getNote())
        {
            $status["n"] = EntityStatus::NOTE_STATUS_INFO;

            $note = trim($note);
            if ( strtoupper(substr($note, 0, 8)) === "WARNING:")
            {
                $status["n"] = EntityStatus::NOTE_STATUS_WARNING;
            }
        }

        if (!$this->address)
            $status["u"] = EntityStatus::ENTITY_STATUS_DANGER;
        elseif (($this->address && $this->address->hasDefaultCoordinates() ||
            !($this->address->getAddress1() && $this->address->getCountry() &&
                    $this->address->getState() && $this->address->getCity())) ||
            !($this->getUsername() && $this->getCell() && $this->getName() &&
                $this->getEmail() && $this->getCustomerNumber()))
            $status["u"] = EntityStatus::ENTITY_STATUS_WARNING;

        $this->setStatus($status);
    }

    /**
     * @return bool Return if the entity general status is ok (often used know if a content can be published)
     */
    public function isOk()
    {
        $const = EntityStatus::ENTITY_STATUS_SUCCESS | EntityStatus::NOTE_STATUS_SUCCESS;
        return ($this->getRawStatus() & $const) == $const;
    }
}
