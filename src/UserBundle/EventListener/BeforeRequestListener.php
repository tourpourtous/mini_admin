<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-06
 * Time: 14:24
 */

namespace UserBundle\EventListener;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class BeforeRequestListener
{
    private $authorizationChecker;
    public function __construct(EntityManagerInterface $em, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->em = $em;
        $this->authorizationChecker = $authorizationChecker;
    }
    public function onKernelRequest(GetResponseEvent $event)
    {
        if ($this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN'))
            return;

        $filter = $this->em
            ->getFilters()
            ->enable('exclude_admins_filter');
        $filter->setParameter('role', "ROLE_ADMIN");
    }
}
