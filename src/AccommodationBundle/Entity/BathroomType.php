<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-29
 * Time: 18:30
 */

namespace AccommodationBundle\Entity;


use AppBundle\Entity\Category;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * Class Bed
 * @ORM\Entity
 * @ORM\Table(name="bathroom_type")
 * @Gedmo\TranslationEntity(class="BathroomTypeTranslation")
 * @UniqueEntity("name")
 */
class BathroomType extends Category
{
    /**
     * @ORM\OneToMany(
     *   targetEntity="BathroomTypeTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"}
     * )
     */
    protected $translations;

    public function useIcon()
    {
        return true;
    }
}
