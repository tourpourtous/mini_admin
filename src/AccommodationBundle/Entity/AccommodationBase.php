<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-27
 * Time: 15:35
 */

namespace AccommodationBundle\Entity;


use AppBundle\Entity\BaseEntity;
use AppBundle\Entity\EntityTraits\TEntityStatus;
use AppBundle\Entity\EntityTraits\TNote;
use AppBundle\Entity\EntityTraits\TPublish;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Intl\Exception\NotImplementedException;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class AccommodationBase
 * @ORM\Entity
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"base" = "AccommodationBase", "accommodation" = "Accommodation", "room" = "Room"})
 * @ORM\Table(name="accommodation_base")
 */
class AccommodationBase extends BaseEntity
{
    use TNote;
    use TEntityStatus;
    use TPublish;

    /**
     * @ORM\ManyToOne(targetEntity="AccommodationTypeCategory")
     * @ORM\JoinColumn(name="rent_type_cat_id", referencedColumnName="id", nullable=false)
     */
    private $rentType;

    /**
     * @ORM\Column(type="integer")
     */
    private $rank;

    /**
     * @ORM\Column(type="integer")
     */
    private $adminRank;

    /**
     * @ORM\ManyToMany(targetEntity="AccommodationOption")
     */
    private $options;

    /**
     * @ORM\Column(type="integer")
     */
    private $defaultPrice=0;

    /**
     * @ORM\ManyToMany(targetEntity="Price", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $prices;

    public function __construct()
    {
        $this->prices = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getRentType()
    {
        return $this->rentType;
    }

    /**
     * @param mixed $rentType
     */
    public function setRentType($rentType)
    {
        $this->rentType = $rentType;
    }

    /**
     * @return mixed
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * @param mixed $rank
     */
    public function setRank($rank)
    {
        $this->rank = $rank;
    }

    /**
     * @return mixed
     */
    public function getAdminRank()
    {
        return $this->adminRank;
    }

    /**
     * @param mixed $adminRank
     */
    public function setAdminRank($adminRank)
    {
        $this->adminRank = $adminRank;
    }

    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param mixed $options
     */
    public function setOptions($options)
    {
        $this->options[] = $options;
    }

    /**
     * @return mixed
     */
    public function getDefaultPrice()
    {
        return $this->defaultPrice;
    }

    /**
     * @param mixed $defaultPrice
     */
    public function setDefaultPrice($defaultPrice)
    {
        $this->defaultPrice = $defaultPrice;
    }

    /**
     * @return mixed
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * Get final price depending on the number of guests
     * @param $from
     * @param $to
     * @param $adultGuests
     * @param $childrenGuests
     * @internal param $guests
     */
    public function getPrice($from, $to, $adultGuests, $childrenGuests)
    {
        throw new NotImplementedException("getPrice Method");
    }

    public function getLowerPrice()
    {
        throw new NotImplementedException("getLowerPrice Method");
    }

    protected function getStatusMapping() {}

    protected function calculateStatus() {}

    public function beforeSave(){}

    /**
     * @return bool Return if the entity general status is ok (often used know if a content can be published)
     */
    public function isOk()
    {
        return true;
    }
}
