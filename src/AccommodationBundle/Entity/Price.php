<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-27
 * Time: 15:49
 */

namespace AccommodationBundle\Entity;


use AppBundle\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Asserts;


/**
 * Class Price
 * @ORM\Entity
 * @ORM\Table(name="price")
 */
class Price extends BaseEntity
{
    /**
     * @ORM\Column(name="from", type="datetimetz")
     * @Asserts\NotBlank(message = "input.not_blank")
     * @Asserts\NotNull()
     */
    public $from;

    /**
     * @ORM\Column(name="to", type="datetimetz")
     * @Asserts\NotBlank(message = "input.not_blank")
     * @Asserts\NotNull()
     */
    public $to;

    /**
     * @ORM\Column(name="price", type="integer")
     * @Asserts\NotBlank(message = "input.not_blank")
     * @Asserts\NotNull()
     */
    public $price;

}
