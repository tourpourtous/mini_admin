<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-28
 * Time: 11:10
 */

namespace AccommodationBundle\Entity;


use AppBundle\Entity\Category;
use AppBundle\Entity\ITranslate;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Asserts;


/**
 * @ORM\Entity
 * @ORM\Table(name="accommodation_service")
 * @Gedmo\TranslationEntity(class="AccommodationServiceTranslation")
 * @UniqueEntity("name")
 */
class AccommodationService extends Category implements ITranslate
{
    /**
     * @ORM\OneToMany(
     *   targetEntity="AccommodationServiceTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"})
     */
    protected $translations;

    public function useIcon()
    {
        return true;
    }
}
