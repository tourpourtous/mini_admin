<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-19
 * Time: 09:00
 */

namespace AccommodationBundle\Entity;


use AppBundle\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Asserts;


/**
 * @ORM\Entity
 * @ORM\Table(name="accommodation_services_relation",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="rent_service_unique_idx", columns={
 *         "accommodation", "option"
 *     })})
 */
class AccommodationServiceRelation extends BaseEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Accommodation", cascade={"persist"})
     * @ORM\JoinColumn(name="accommodation", referencedColumnName="id")
     */
    private $accommodation;

    /**
     * @ORM\ManyToOne(targetEntity="AccommodationOption", cascade={"persist"})
     * @ORM\JoinColumn(name="option", referencedColumnName="id")
     */
    private $option;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="boolean")
     */
    private $perPerson;

    public function __construct($accommodation, $option, $price)
    {
        $this->accommodation = $accommodation;
        $this->option = $option;
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getAccommodation()
    {
        return $this->accommodation;
    }

    /**
     * @return mixed
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getIsPerPerson()
    {
        return $this->perPerson;
    }

    /**
     * @return mixed
     */
    public function setIsPerPerson($value)
    {
        $this->perPerson = $value;
    }
}
