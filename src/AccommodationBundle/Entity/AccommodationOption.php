<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-28
 * Time: 11:10
 */

namespace AccommodationBundle\Entity;


use AppBundle\Entity\BaseEntity;
use AppBundle\Entity\Category;
use AppBundle\Entity\EntityTraits\TExtra;
use AppBundle\Entity\EntityTraits\TTranslatableName;
use AppBundle\Entity\EntityTraits\TTranslate;
use AppBundle\Entity\ITranslate;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Asserts;


/**
 * @ORM\Table(name="accommodation_option")
 * @ORM\Entity(repositoryClass="AccommodationBundle\Repository\AccommodationOptionRepository")
 * @Gedmo\TranslationEntity(class="AccommodationOptionTranslation")
 * @UniqueEntity("name")
 */
class AccommodationOption extends Category implements ITranslate
{
    use TExtra;

    const ACCOMMODATION = AccommodationTypeCategory::ACCOMMODATION;
    const ROOM = AccommodationTypeCategory::ROOM;

    const GROUP_CONDITIONS = 4;
    const GROUP_ALLOWS = 8;
    const GROUP_SAFETY = 16;
    const GROUP_AMENITIES = 32;

    const GROUP_CONTAINER_CONDITIONS_ROUTE_ID = "accommodation-options-conditions";
    const GROUP_ROOM_CONDITIONS_ROUTE_ID = "room-options-conditions";
    const GROUP_ALLOWS_ROUTE_ID = "accommodation-options-allows";
    const GROUP_CONTAINER_SAFETY_ROUTE_ID = "accommodation-options-safety";
    const GROUP_ROOM_SAFETY_ROUTE_ID = "room-options-safety";
    const GROUP_ROOM_AMENITIES_ROUTE_ID = "room-options-amenities";

    /**
     * @ORM\OneToMany(
     *   targetEntity="AccommodationOptionTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"})
     */
    protected $translations;

    public function __construct($group=0, $accommodation=true)
    {
        parent::__construct();

        $this->setGroups($group, $accommodation);
    }

    public function hasGroups($groups, $accommodation=true)
    {
        $rent_group = $accommodation ? self::ACCOMMODATION : self::ROOM;
        return boolval($this->getExtra($rent_group|$groups));
    }

    public function setGroups($groups, $rentContainer=true)
    {
        $rent_group = $rentContainer ? self::ACCOMMODATION : self::ROOM;
        $this->setExtra($rent_group|$groups, true);
    }

    public function unsetGroups($groups, $rentContainer=true)
    {
        $rent_group = $rentContainer ? self::ACCOMMODATION : self::ROOM;
        $this->setExtra($rent_group|$groups, false);
    }

    public function useIcon()
    {
        return true;
    }
}
