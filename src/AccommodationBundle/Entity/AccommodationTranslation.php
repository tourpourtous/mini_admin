<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-10
 * Time: 19:34
 */

namespace AccommodationBundle\Entity;


use AppBundle\Entity\BaseTranslation;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="accommodation_cont_translations",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="src_tr_unique_idx", columns={
 *         "locale", "object_id", "field"
 *     })}
 * )
 */
class AccommodationTranslation extends BaseTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="Accommodation", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $object;
}
