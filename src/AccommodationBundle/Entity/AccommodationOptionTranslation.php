<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-28
 * Time: 11:55
 */

namespace AccommodationBundle\Entity;


use AppBundle\Entity\BaseTranslation;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity()
 * @ORM\Table(name="accommodation_opt_translations",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="rent_opt_tr_unique_idx", columns={
 *         "locale", "object_id", "field"
 *     })}
 * )
 */
class AccommodationOptionTranslation extends BaseTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="AccommodationOption", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $object;

}
