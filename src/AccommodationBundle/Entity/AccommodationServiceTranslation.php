<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-28
 * Time: 11:55
 */

namespace AccommodationBundle\Entity;


use AppBundle\Entity\BaseTranslation;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="accommodation_service_translations",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="rent_serv_tr_unique_idx", columns={
 *         "locale", "object_id", "field"
 *     })}
 * )
 */
class AccommodationServiceTranslation extends BaseTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="AccommodationService", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $object;

}
