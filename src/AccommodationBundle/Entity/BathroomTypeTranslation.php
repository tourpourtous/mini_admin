<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-29
 * Time: 18:36
 */

namespace AccommodationBundle\Entity;


use AppBundle\Entity\BaseTranslation;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="bathroom_type_translations",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="bath_type_tr_unique_idx", columns={
 *         "locale", "object_id", "field"
 *     })}
 * )
 */
class BathroomTypeTranslation extends BaseTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="BathroomType", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $object;

}