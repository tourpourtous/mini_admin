<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-18
 * Time: 16:19
 */

namespace AccommodationBundle\Entity;


use AppBundle\Entity\EntityTraits\TAddress;
use AppBundle\Entity\EntityTraits\TContactInfo;
use AppBundle\Entity\EntityTraits\TCustomerNumber;
use AppBundle\Entity\EntityTraits\TDestination;
use AppBundle\Entity\EntityTraits\TExtra;
use AppBundle\Entity\EntityTraits\TName;
use AppBundle\Entity\EntityTraits\TTranslatableDescription;
use AppBundle\Entity\EntityTraits\TTranslate;
use AppBundle\Entity\ITranslate;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Asserts;


/**
 * @ORM\Entity(repositoryClass="AccommodationBundle\Repository\AccommodationRepository")
 * @ORM\Table(name="accommodation")
 * @Gedmo\TranslationEntity(class="AccommodationTranslation")
 */
class Accommodation extends AccommodationBase implements ITranslate
{
    const REGISTERED = 1;
    const SIGNED = 2;

    use TName;
    use TContactInfo;
    use TAddress;
    use TDestination;
    use TTranslatableDescription;
    use TExtra;
    use TCustomerNumber;
    use TTranslate{
        __construct as private initialize;
    }

    /**
     * @ORM\Column(type="string", length=255)
     * @Asserts\NotBlank(message = "input.not_blank")
     * @Asserts\NotNull()
     */
    protected $name="";

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $owner;

    /**
     * @ORM\Column(type="string", length=10, unique=true, nullable=true)
     * @Asserts\NotBlank(message = "input.not_blank")
     */
    private $customerId;

    /**
     * @ORM\OneToMany(
     *   targetEntity="AccommodationTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * @ORM\Column(type="time")
     */
    private $checkInTime;

    /**
     * @ORM\Column(type="time")
     */
    private $checkOutTime;

    /**
     * @ORM\OneToMany(targetEntity="AccommodationServiceRelation", mappedBy="accommodation", cascade={"remove"})
     */
    private $services;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Language")
     */
    private $languages;

    /**
     * @ORM\ManyToMany(targetEntity="ContentBundle\Entity\Place")
     */
    private $placesOfInterest;

    /**
     * @ORM\OneToMany(targetEntity="Room", mappedBy="container", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $pieces;

    public function __construct()
    {
        $this->initialize();
        parent::__construct();

        $this->languages = new ArrayCollection();
        $this->placesOfInterest = new ArrayCollection();
        $this->pieces = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return mixed
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param mixed $customerId
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    public function getTranslatableFields()
    {
        return ["description", "note"];
    }

    /**
     * @return mixed
     */
    public function getCheckInTime()
    {
        return $this->checkInTime;
    }

    /**
     * @param mixed $checkInTime
     */
    public function setCheckInTime($checkInTime)
    {
        $this->checkInTime = $checkInTime;
    }

    /**
     * @return mixed
     */
    public function getCheckOutTime()
    {
        return $this->checkOutTime;
    }

    /**
     * @param mixed $checkOutTime
     */
    public function setCheckOutTime($checkOutTime)
    {
        $this->checkOutTime = $checkOutTime;
    }

    /**
     * @return mixed
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @param mixed $services
     */
    public function setServices($services)
    {
        $this->services = $services;
    }

    /**
     * @return boolean
     */
    public function getRegistered(): bool
    {
        return boolval($this->getExtra(self::REGISTERED));
    }

    /**
     * @param bool $value
     */
    public function setRegistered(bool $value=true)
    {
        $this->setExtra(self::REGISTERED, $value);
    }

    /**
     * @return boolean
     */
    public function getSigned(): bool
    {
        return boolval($this->getExtra(self::SIGNED));
    }

    /**
     * @param bool $value
     */
    public function setSigned(bool $value=true)
    {
        $this->setExtra(self::SIGNED, $value);
    }

    /**
     * @return mixed
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param mixed $languages
     */
    public function setLanguages($languages)
    {
        $this->languages->add($languages);
    }

    /**
     * @param mixed $languages
     */
    public function unsetLanguages($languages)
    {
        $this->languages->remove($languages);
    }

    /**
     * @return mixed
     */
    public function getPlacesOfInterest()
    {
        return $this->placesOfInterest;
    }

    /**
     * @param mixed $placesOfInterest
     */
    public function setPlacesOfInterest($placesOfInterest)
    {
        $this->placesOfInterest->add($placesOfInterest);
    }
    /**
     * @param mixed $placesOfInterest
     */
    public function unsetPlacesOfInterest($placesOfInterest)
    {
        $this->placesOfInterest->remove($placesOfInterest);
    }

    /**
     * @return mixed
     */
    public function getPieces()
    {
        return $this->pieces;
    }

    /**
     * @param mixed $pieces
     */
    public function setPieces($pieces)
    {
        $this->pieces->add($pieces);
    }

    /**
     * @param mixed $pieces
     */
    public function unsetPieces($pieces)
    {
        $this->pieces->remove($pieces);
    }
}
