<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-19
 * Time: 09:00
 */

namespace AccommodationBundle\Entity;


use AppBundle\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Asserts;


/**
 * @ORM\Entity
 * @ORM\Table(name="room_beds_relation",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="room_bed_unique_idx", columns={
 *         "room", "bed_type"
 *     })})
 */
class RoomBedTypeRelation extends BaseEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Room", cascade={"persist"})
     * @ORM\JoinColumn(name="room", referencedColumnName="id")
     */
    private $room;

    /**
     * @ORM\ManyToOne(targetEntity="BedType", cascade={"persist"})
     * @ORM\JoinColumn(name="bed_type", referencedColumnName="id")
     */
    private $bedType;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    public function __construct($accommodation, $bedType, $quantity)
    {
        $this->room = $accommodation;
        $this->bedType = $bedType;
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * @return mixed
     */
    public function getBedType()
    {
        return $this->bedType;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
}
