<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-27
 * Time: 16:03
 */

namespace AccommodationBundle\Entity;


use Doctrine\ORM\Mapping as ORM;


/**
 * Class Room
 * @ORM\Entity
 * @ORM\Table(name="room")
 */
class Room extends AccommodationBase
{
    /**
     * @ORM\Column(type="integer")
     */
    private $roomNumber;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isHalfFloor;

    /**
     * @ORM\Column(type="boolean")
     */
    private $independentEntrance;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberOfGuests;

    /**
     * @ORM\ManyToOne(targetEntity="Accommodation", inversedBy="pieces")
     */
    private $container;

    /**
     * @ORM\OneToMany(targetEntity="RoomBedTypeRelation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"})
     */
    private $beds;

    /**
     * @ORM\ManyToOne(targetEntity="BathroomType")
     * @ORM\JoinColumn(name="bathroom_id", referencedColumnName="id")
     */
    private $bathroom;

    public function getOwner()
    {
        return $this->getContainer()->getOwner();
    }

    /**
     * @return boolean
     */
    public function IsHalfFloor()
    {
        return $this->isHalfFloor;
    }

    /**
     * @param boolean $value
     */
    public function setIsHalfFloor($value)
    {
        $this->isHalfFloor = $value;
    }

    /**
     * @return boolean
     */
    public function getIndependentEntrance()
    {
        return $this->independentEntrance;
    }

    /**
     * @param boolean $independentEntrance
     */
    public function setIndependentEntrance($independentEntrance)
    {
        $this->independentEntrance = $independentEntrance;
    }

    /**
     * @return integer
     */
    public function getRoomNumber()
    {
        return $this->roomNumber;
    }

    /**
     * @param integer $roomNumber
     */
    public function setRoomNumber($roomNumber)
    {
        $this->roomNumber = $roomNumber;
    }

    /**
     * @return integer
     */
    public function getNumberOfGuests()
    {
        return $this->numberOfGuests;
    }

    /**
     * @param integer $numberOfGuests
     */
    public function setNumberOfGuests($numberOfGuests)
    {
        $this->numberOfGuests = $numberOfGuests;
    }

    /**
     * @return mixed
     */
    public function getBeds()
    {
        return $this->beds;
    }

    /**
     * @param mixed $beds
     */
    public function setBeds($beds)
    {
        $this->beds = $beds;
    }

    /**
     * @param mixed $beds
     */
    public function unsetBed($beds)
    {
        $this->beds = $beds;
    }

    /**
     * @return mixed
     */
    public function getBathroom()
    {
        return $this->bathroom;
    }

    /**
     * @param mixed $bathroom
     */
    public function setBathroom($bathroom)
    {
        $this->bathroom = $bathroom;
    }

    /**
     * @return Accommodation
     */
    public function getContainer(): Accommodation
    {
        return $this->container;
    }

    /**
     * @param Accommodation $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * @return mixed
     */
    public function getDefaultPrice()
    {
        return parent::getDefaultPrice();
    }

    /**
     * @return mixed
     */
    public function getPrices()
    {
        return parent::getPrices();
    }

    /**
     * Get final price depending on the number of guests
     * @param $from
     * @param $to
     * @param $adultGuests
     * @param $childrenGuests
     * @internal param $guests
     */
    public function getPrice($from, $to, $adultGuests, $childrenGuests)
    {
        throw new NotImplementedException("getPrice Method");
    }

    public function getLowerPrice()
    {
        throw new NotImplementedException("getPrice Method");
    }

    protected function getStatusMapping()
    {
        throw new NotImplementedException("getStatusMapping");
    }

    /**
     * Function defined in child class since is the one who knows how is calculated the status.
     *
     * @return void
     */
    protected function calculateStatus()
    {
        throw new NotImplementedException("calculateStatus");
    }
}
