<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-27
 * Time: 11:15
 */

namespace AccommodationBundle\Entity;


use AppBundle\Entity\Category;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class AccommodationTypeCategory
 * @ORM\Entity(repositoryClass="AccommodationBundle\Repository\AccommodationTypeCategoryRepository")
 * @ORM\Table(name="accommodation_category")
 * @Gedmo\TranslationEntity(class="AccommodationTypeCategoryTranslation")
 * @UniqueEntity("name")
 */
class AccommodationTypeCategory extends Category
{
    const ACCOMMODATION = 1;
    const ROOM = 2;

    const CONTAINER_ROUTE_ID = "accommodation";
    const PIECE_ROUTE_ID = "room";

    /**
     * @ORM\OneToMany(
     *   targetEntity="AccommodationTypeCategoryTranslation",
     *   mappedBy="object",
     *   cascade={"persist", "remove"})
     */
    protected $translations;

    public function __construct($rentType=self::ACCOMMODATION)
    {
        parent::__construct();
    }

    /**
     * @ORM\Column(type="boolean")
     */
    private $isContainer;

    /**
     * @param integer $rentType
     */
    public function setRentType($rentType)
    {
        $this->isContainer = $rentType == self::ACCOMMODATION;
    }

    /**
     * @return mixed
     */
    public function getRentType()
    {
        return $this->isContainer ? self::ACCOMMODATION : self::ROOM;
    }
}
