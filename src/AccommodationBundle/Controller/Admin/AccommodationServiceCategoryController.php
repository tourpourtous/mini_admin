<?php

namespace AccommodationBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


/**
 * Class AccommodationServiceCategoryController
 * @package AccommodationBundle\Controller\Admin
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */
class AccommodationServiceCategoryController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('AccommodationBundle:Default:index.html.twig');
    }
}
