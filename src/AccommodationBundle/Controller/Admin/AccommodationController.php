<?php

namespace AccommodationBundle\Controller\Admin;

use AccommodationBundle\Entity\Accommodation;
use AccommodationBundle\Entity\AccommodationOption;
use AccommodationBundle\Entity\AccommodationService;
use AccommodationBundle\Entity\AccommodationTypeCategory;
use AccommodationBundle\Service\AccommodationControllerService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class AccommodationController extends Controller
{
    /**
     * @Route("/", name="admin_accommodation_list")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $entities = $this->getDoctrine()
            ->getRepository(Accommodation::class)
            ->findAll();

        return $this->render('AccommodationBundle:admin/accommodation:list.html.twig', ["items" => $entities]);
    }

    /**
     * @Route("/{id}/", name="admin_accommodation_show", requirements={"id" = "\d+"})
     * @param $pageType
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function showAction($pageType, $id)
    {
        $repository = $this->getDoctrine()
            ->getRepository(Accommodation::class);

        $entity = $repository->find($id);

        if (!$entity || $entity->getPageType()->getCode() != $pageType) {
            $this->addFlash("warn", "message.get.error");
            return $this->redirectToRoute('admin_accommodation_list', ["pageType" => $pageType]);
        }

        $translations = $repository->versions($entity);

        return $this->render('AccommodationBundle:admin/accommodation:details.html.twig',
            [
                "entity" => $entity,
                "form_data" => [
                    "prev" => $repository->findPrev($entity),
                    "next" => $repository->findNext($entity),
                    "translations" => $translations
                ]
            ]
        );
    }

    /**
     * @Route("/new/", name="admin_accommodation_new")
     * @Method({"GET", "POST"})
     * @param AccommodationControllerService $accommodationPageService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(AccommodationControllerService $accommodationPageService)
    {
        $token = 'new_accommodation';

        $repository = $this->getDoctrine()
            ->getRepository(Accommodation::class);

        $entity = new Accommodation();

        $category_repository = $this->getDoctrine()
            ->getRepository(AccommodationTypeCategory::class);

        $service_repository = $this->getDoctrine()
            ->getRepository(AccommodationService::class);

        $option_repository = $this->getDoctrine()
            ->getRepository(AccommodationOption::class);

        return $accommodationPageService->save($repository, $category_repository, $service_repository, $option_repository, $entity, $token);
    }

    /**
     * @Route("/{id}/edit/", name="admin_accommodation_edit", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     * @param $pageType
     * @param $id
     * @param AccommodationControllerService $accommodationPageService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($pageType, $id, AccommodationControllerService $accommodationPageService)
    {
//        $token = "edit_accommodation";
//
//        $repository = $this->getDoctrine()
//            ->getRepository(Accommodation::class);
//
//        $entity = $repository->find($id);
//
//        if (!$entity || $entity->getPageType()->getCode() != $pageType) {
//            $this->addFlash("warn", "message.get.error");
//            return $this->redirectToRoute('admin_accommodation_list', ["pageType" => $pageType]);
//        }
//
//        $categories_repository = $this->getDoctrine()
//            ->getRepository(AccommodationCategory::class);
//
//        return $accommodationPageService->save($repository, $entity, $categories_repository, $token);
    }

    /**
     * @Route("/{id}/delete/", name="admin_accommodation_delete", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     * @param $pageType
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($pageType, $id, Request $request)
    {
        $repository = $this->getDoctrine()
            ->getRepository(Accommodation::class);

        $entity = $repository->find($id);

        if (!$entity) {
            $this->addFlash("warn", "message.get.error");
            return $this->redirectToRoute('admin_accommodation_list', ["pageType" => $pageType]);
        }
        try {
            if ($request->getMethod() == 'POST') {
                $submittedToken = $request->request->get("_csrf_token");
                if (!$this->isCsrfTokenValid('delete_accommodation', $submittedToken)) {
                    $this->addFlash(
                        'error',
                        'message.form.invalid'
                    );
                } else {
                    $repository->delete($entity);

                    $this->addFlash(
                        'success',
                        'message.delete.success'
                    );
                }

                return $this->redirectToRoute('admin_accommodation_list', ["pageType" => $pageType]);
            }
        } catch (\Exception $exc) {
            $this->addFlash(
                'error',
                'message.delete.error'
            );
        }

        $translations = $repository->versions($entity);

        return $this->render('AccommodationBundle:admin/accommodation:delete.html.twig',
            [
                "entity" => $entity,
                "form_data" => [
                    "prev" => $repository->findPrev($entity),
                    "next" => $repository->findNext($entity),
                    "translations" => $translations
                ]
            ]
        );
    }
}
