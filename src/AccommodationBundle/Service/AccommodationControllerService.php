<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-04
 * Time: 12:00
 */

namespace AccommodationBundle\Service;


use AccommodationBundle\Entity\Accommodation;
use AccommodationBundle\Entity\AccommodationOption;
use AccommodationBundle\Repository\AccommodationOptionRepository;
use AccommodationBundle\Repository\AccommodationTypeCategoryRepository;
use AppBundle\Entity\EntityTraits\TControllerAwareService;
use AppBundle\Repository\BaseEntityRepository;
use AppBundle\Repository\TranslationRepository;


class AccommodationControllerService
{
    use TControllerAwareService;

    public function save(TranslationRepository $repository,
                         AccommodationTypeCategoryRepository $categoryRepository,
                         BaseEntityRepository $serviceRepository,
                         AccommodationOptionRepository $optionRepository,
                         Accommodation $entity, $token=null)
    {
        $default_locale = $this->getParameter("locale");
        $locales = $this->getParameter("locales");

        $translations = [];
        $request = $this->getCurrentRequest();
        try {
            if ($request->getMethod() == 'POST')
            {
                $submittedToken = $request->request->get("_csrf_token");
                if (!$this->isCsrfTokenValid($token, $submittedToken)) {
                    $this->addFlash(
                        'error',
                        'message.form.invalid'
                    );

                    return $this->redirectToRoute('admin_infopage_list');
                }

                $translations = $request->request->get("translations");

                $name = trim($translations[$default_locale]["name"]);
                $description = trim($translations[$default_locale]["description"]);
                $note = trim($translations[$default_locale]["note"]);

                $removed_categories = [];

                foreach ($removed_categories as $key => $removed_category)
                {
                    $entity->deleteCategory($key);
                    $_result = $repository->deleteCategory($entity, $removed_category);
                }

                $entity->setPublished($request->request->get("publish"));
                $entity->setName($name);
                $entity->setDescription($description);
                $entity->setNote($note);

                if (count($this->validate($entity))) {
                    $this->addFlash(
                        'error',
                        'message.form.empty'
                    );
                }
                else
                {
                    unset($translations[$default_locale]);

                    foreach ($translations as $locale=>$translation)
                    {
                        foreach ($translation as $field=>$_value)
                        {
                            if (!in_array($locale, $locales))
                            {
                                continue;
                            }

                            $value = trim($_value);
                            if ($value)
                            {
                                $repository->translate($entity, $field, $locale, $value);
                            }
                            else
                            {
                                $repository->deleteTranslation($entity, $field, $locale);
                            }
                        }
                    }

                    $repository->save($entity);

                    $this->addFlash(
                        'success',
                        'message.save.success'
                    );
                    return $this->redirectToRoute('admin_accommodation_show', ["id" => $entity->getId()]);
                }
            }
        }catch (\Exception $exc)
        {
            $this->addFlash(
                'error',
                'message.edit.error'
            );
        }

        if (!$translations && $token == "edit_info")
            $translations = $repository->versions($entity, True);

        return $this->render('AccommodationBundle:admin/accommodation:input.html.twig', [
            "entity" => $entity,
            "token" => $token,
            "services" => $serviceRepository->findAll(),
            "conditions" => $optionRepository->findByFilter(AccommodationOption::GROUP_CONTAINER_CONDITIONS_ROUTE_ID),
            "safety" => $optionRepository->findByFilter(AccommodationOption::GROUP_CONTAINER_SAFETY_ROUTE_ID),
            "allows" => $optionRepository->findByFilter(AccommodationOption::GROUP_ALLOWS_ROUTE_ID),
            "form_data" => [
                "translations" => $translations]
        ]);
    }
}