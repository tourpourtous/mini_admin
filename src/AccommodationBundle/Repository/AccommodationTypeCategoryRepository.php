<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-10-01
 * Time: 22:36
 */

namespace AccommodationBundle\Repository;


use AppBundle\Repository\CategoryRepository;
use AccommodationBundle\Entity\AccommodationTypeCategory;


class AccommodationTypeCategoryRepository extends CategoryRepository
{
    protected function getFilter($key)
    {
        $filters = [
            AccommodationTypeCategory::CONTAINER_ROUTE_ID => [["query"=> 'e.isContainer = :v', "param"=>["key"=>"v", "value"=>true]]],
            AccommodationTypeCategory::PIECE_ROUTE_ID => [["query"=> 'e.isContainer = :v', "param"=>["key"=>"v", "value"=>false]]],
        ];

        return $filters[$key];
    }
}
