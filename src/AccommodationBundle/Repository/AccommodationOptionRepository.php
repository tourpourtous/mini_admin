<?php

namespace AccommodationBundle\Repository;


use AppBundle\Entity\IBaseEntity;
use AppBundle\Repository\CategoryRepository;
use AccommodationBundle\Entity\AccommodationOption;


/**
 * class AccommodationOptionRepository
 */
class AccommodationOptionRepository extends CategoryRepository
{
//    protected function getIgnoredAttributes(): array{}
//
//    protected function getNormalizerCallbacks($locale){}

    protected function getFilter($key)
    {
        $query = 'BIT_AND(e.extra, :v) = :v';
        $filters = [
            AccommodationOption::GROUP_CONTAINER_CONDITIONS_ROUTE_ID => [["query"=> $query, "param"=>["key"=>"v", "value"=>AccommodationOption::ACCOMMODATION|AccommodationOption::GROUP_CONDITIONS]]],
            AccommodationOption::GROUP_ROOM_CONDITIONS_ROUTE_ID => [["query"=> $query, "param"=>["key"=>"v", "value"=>AccommodationOption::ACCOMMODATION|AccommodationOption::GROUP_CONDITIONS]]],
            AccommodationOption::GROUP_ALLOWS_ROUTE_ID => [["query"=> $query, "param"=>["key"=>"v", "value"=>AccommodationOption::ACCOMMODATION|AccommodationOption::GROUP_ALLOWS]]],
            AccommodationOption::GROUP_CONTAINER_SAFETY_ROUTE_ID => [["query"=> $query, "param"=>["key"=>"v", "value"=>AccommodationOption::ACCOMMODATION|AccommodationOption::GROUP_SAFETY]]],
            AccommodationOption::GROUP_ROOM_SAFETY_ROUTE_ID => [["query"=> $query, "param"=>["key"=>"v", "value"=>AccommodationOption::ROOM|AccommodationOption::GROUP_SAFETY]]],
            AccommodationOption::GROUP_ROOM_AMENITIES_ROUTE_ID => [["query"=> $query, "param"=>["key"=>"v", "value"=>AccommodationOption::ROOM|AccommodationOption::GROUP_AMENITIES]]],
        ];

        return $filters[$key];
    }

    public function save(IBaseEntity $entity)
    {
        if ($_entity = $this->findOneBy(["name" => $entity->getName()]))
        {
            $_entity->setExtra($entity->getRawExtra());
            $entity = $_entity;
        }

        parent::save($entity);
    }
}
