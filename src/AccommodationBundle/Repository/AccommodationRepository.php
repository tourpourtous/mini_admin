<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-25
 * Time: 21:32
 */

namespace AccommodationBundle\Repository;


use AppBundle\Entity\ContactInfo;
use AppBundle\Entity\IBaseEntity;
use AppBundle\Repository\TranslationRepository;
use AccommodationBundle\Entity\Accommodation;


class AccommodationRepository extends TranslationRepository
{
//    protected function getIgnoredAttributes(): array
//    {
//        return array_merge(parent::getIgnoredAttributes(), ["pageType"]);
//    }

//    protected function getNormalizerCallbacks($locale){}

    public function save(IBaseEntity $entity)
    {
        //Verify if a contact record exist with this data. If so, change the current by the one found
        if (($contact = $entity->getContact()) && !$contact->getId())
        {
            $new_contact = $this->getEntityManager()->getRepository(ContactInfo::class)->findOneBy([
                "email" => $contact->getEmail(),
                "cellNumber" => $contact->getCellNumber(),
                "landNumber" => $contact->getLandNumber()
            ]);

            $entity->setContact($new_contact ?? $contact);
        }

        parent::save($entity);
    }
}