<?php
/**

 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-15
 * Time: 13:56
 */

namespace AppBundle\Controller\API;

use AppBundle\Service\HelperService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ApiV1 extends Controller
{
    /**
     * @Route("/get_translation/{type}/{locale}/{id}", name="api_get_translation", requirements={"type" = "destination|place", "id" = "\d+"})
     * @Method({"GET"})
     */
//     * @Security("has_role('ROLE_ADMIN')")
    public function getTranslation($type, $locale, $id, HelperService $hs)
    {
        $translation = [];

        try{
            $translation = $hs->getTranslationsHelper($type, $id)[$locale];
        }
        catch (\ErrorException $exc){}
        catch (Exception $exc){}

        return $this->json($translation);
    }

    /**
     * @Route("/get_translations/{type}/{id}", name="api_get_translations", requirements={"id" = "\d+"})
     * @Method({"GET"})
     */
//     * @Security("has_role('ROLE_ADMIN')")
    public function getTranslations($type, $id, HelperService $hs)
    {
        return $this->json($hs->getTranslationsHelper($type, $id));
    }
}
