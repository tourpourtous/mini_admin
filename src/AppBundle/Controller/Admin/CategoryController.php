<?php

namespace AppBundle\Controller\Admin;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\CategoryService;


/**
 * Class UserController
 * @package UsersBundle\Controller\Admin
 */
class CategoryController extends Controller
{
    private function getClass($_category)
    {
        $categoryClass = $this->get($_category."_category_class");
        return (new \ReflectionClass($categoryClass))->name;
    }

    /**
     * @Route("/", name="admin_category_list")
     * @param $_category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction($_category)
    {
        $entities = $this->getDoctrine()
            ->getRepository($this->getClass($_category))
            ->findByFilter($_category);

        return $this->render('AppBundle:admin/category:list.html.twig', ["category" => $_category, "items" => $entities]);
    }

    /**
     * @Route("/{id}/", name="admin_category_show", requirements={"id" = "\d+"})
     * @param $_category
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function showAction($_category, $id)
    {
        $repository = $this->getDoctrine()
            ->getRepository($this->getClass($_category));

        $entity = $repository->find($id);

        if (!$entity) {
            $this->addFlash("warn", "message.get.error");
            return $this->redirectToRoute('admin_category_list', ["category" => $_category]);
        }

        $translations = $repository->versions($entity);

        return $this->render('AppBundle:admin/category:details.html.twig',
            [
                "entity"=> $entity,
                "category" => $_category,
                "form_data" => [
                    "prev" => $repository->findPrev($entity),
                    "next" => $repository->findNext($entity),
                    "translations" => $translations
                ]
            ]
        );
    }

    /**
     * @Route("/new/", name="admin_category_new")
     * @Method({"GET", "POST"})
     * @param $_category
     * @param CategoryService $categoryService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction($_category, CategoryService $categoryService)
    {
        $token = 'new_category';

        $entity = $this->get($_category."_category_class");
        $categoryClass = (new \ReflectionClass($entity))->name;

        $repository = $this->getDoctrine()
            ->getRepository($categoryClass);

        return $categoryService->save($_category, $repository, $entity, $token);
    }

    /**
     * @Route("/{id}/edit/", name="admin_category_edit", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     * @param $_category
     * @param $id
     * @param CategoryService $categoryService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($_category, $id, CategoryService $categoryService)
    {
        $token = "edit_category";

        $repository = $this->getDoctrine()
            ->getRepository($this->getClass($_category));

        $entity = $repository->find($id);

        if (!$entity) {
            $this->addFlash("warn", "message.get.error");
            return $this->redirectToRoute('admin_category_list', ["category" => $_category]);
        }

        return $categoryService->save($_category, $repository, $entity, $token);
    }

    /**
     * @Route("/{id}/delete/", name="admin_category_delete", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     * @param $_category
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($_category, $id, Request $request)
    {
        $repository = $this->getDoctrine()
            ->getRepository($this->getClass($_category));

        $entity = $repository->find($id);

        if (!$entity) {
            $this->addFlash("warn", "message.get.error");
            return $this->redirectToRoute('admin_category_list', ["_category" => $_category]);
        }
        try {
            if ($request->getMethod() == 'POST')
            {
                $submittedToken = $request->request->get("_csrf_token");
                if (!$this->isCsrfTokenValid('delete_category', $submittedToken)) {
                    $this->addFlash(
                        'error',
                        'message.form.invalid'
                    );
                }
                else
                {
                    $repository->delete($entity);

                    $this->addFlash(
                        'success',
                        'message.delete.success'
                    );
                }

                return $this->redirectToRoute('admin_category_list', ["_category" => $_category]);
            }
        }catch (\Exception $exc)
        {
            $this->addFlash(
                'error',
                'message.delete.error'
            );
        }

        $translations = $repository->versions($entity);

        return $this->render('AppBundle:admin/category:delete.html.twig',
            [
                "entity"=> $entity,
                "category" => $_category,
                "form_data" => [
                    "prev" => $repository->findPrev($entity),
                    "next" => $repository->findNext($entity),
                    "translations" => $translations
                ]
            ]
        );
    }
}
