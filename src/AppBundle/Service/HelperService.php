<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-17
 * Time: 12:07
 */

namespace AppBundle\Service;


use Doctrine\ORM\EntityManager;
use Gedmo\Translatable\Translatable;
use Gedmo\Translatable\Entity\Translation;

class HelperService
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function capitalize($string){
        $charset = "UTF-8";
        return mb_strtoupper(mb_substr($string, 0, 1, $charset), $charset).mb_strtolower(mb_substr($string, 1, null, $charset), $charset);
    }

    public function getTranslationsHelper($type, $id)
    {
        $repository = $this->em->getRepository("AppBundle\\Entity\\". $this->capitalize($type));
        $entity = $repository->find($id);

        if(!$entity){
            return [];
        }

        if($entity instanceof Translatable){
            $repository = $this->em->getRepository(Translation::class);
        }

        return $repository->findTranslations($entity);
    }

}
