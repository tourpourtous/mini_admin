<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-04
 * Time: 12:00
 */

namespace AppBundle\Service;


use AppBundle\Entity\Category;
use AppBundle\Entity\EntityTraits\TControllerAwareService;
use AppBundle\Repository\TranslationRepository;


class CategoryService
{
    use TControllerAwareService;

    public function save($_category, TranslationRepository $repository, Category $entity, $token=null)
    {
        $default_locale = $this->getParameter("locale");
        $locales = $this->getParameter("locales");

        $translations = [];
        $request = $this->getCurrentRequest();
        try {
            if ($request->getMethod() == 'POST')
            {
                $submittedToken = $request->request->get("_csrf_token");
                if (!$this->isCsrfTokenValid($token, $submittedToken)) {
                    $this->addFlash(
                        'error',
                        'message.form.invalid'
                    );

                    return $this->redirectToRoute('admin_category_list', ["_category" => $_category]);
                }

                $translations = $request->request->get("translations");
                $icon = $request->request->get("icon");

                $name = trim($translations[$default_locale]["name"]);
                $entity->setName($name);
                $entity->setIcon($icon);

                if (count($this->validate($entity))) {
                    $this->addFlash(
                        'error',
                        'message.form.empty'
                    );
                }
                else
                {
                    unset($translations[$default_locale]);

                    foreach ($translations as $locale=>$translation)
                    {
                        foreach ($translation as $field=>$_value)
                        {
                            if (!in_array($locale, $locales))
                            {
                                continue;
                            }

                            $value = trim($_value);
                            if ($value)
                            {
                                $repository->translate($entity, $field, $locale, $value);
                            }
                            else
                            {
                                $repository->deleteTranslation($entity, $field, $locale);
                            }
                        }
                    }

                    $repository->save($entity);

                    $this->addFlash(
                        'success',
                        'message.save.success'
                    );
                    return $this->redirectToRoute('admin_category_show', ["_category" => $_category, "id" => $entity->getId()]);
                }
            }
        }catch (\Exception $exc)
        {
            $message = 'message.save.error';
            if ($token == "edit_category")
            {
                $message = 'message.edit.error';
            }

            $this->addFlash(
                'error',
                $message
            );
        }

        if (!$translations && $token == "edit_category")
            $translations = $repository->versions($entity, True);

        return $this->render('AppBundle:admin/category:input.html.twig', [
            "entity" => $entity,
            "category" => $_category,
            "token" => $token,
            "form_data" => [
                "translations" => $translations]
        ]);
    }
}