<?php
/**
 * Created by Ernesto Baez.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-09
 * Time: 19:46
 */

namespace AppBundle\Repository;

use AppBundle\Entity\City;
use AppBundle\Entity\Country;
use Doctrine\ORM\Query;

class StateRepository extends BaseEntityRepository
{
    public function findAllByCountryCode($code)
    {
        $result = [];
        try{
            $country = $country_repository = $this->getEntityManager()->getRepository(Country::class)->findOneBy(["code" => $code]);

            $entities = $this->findBy(["country" => $country]);

            foreach ($entities as $entity)
            {
                $result[] = ["id" => $entity->getId(), "name" => $entity->getName()];
            }

        }catch (\Exception $exc){}

        return $result;
    }

    public function statesCitiesTree($countryCode)
    {
        $result = [];
        try{
            $country = $country_repository = $this->getEntityManager()->getRepository(Country::class)->findOneBy(["code" => $countryCode]);
            $states = $this->findBy(["country" => $country]);

            foreach ($states as $state)
            {
                $cities = $this->_em->createQueryBuilder()
                    ->select('city.id, city.name')
                    ->from(City::class, 'city')
                    ->where("city.state = :state")
                    ->getQuery()
                    ->execute(
                        compact('state'),
                        Query::HYDRATE_ARRAY
                    );

                $result[] = ["id" => $state->getId(), "name" => $state->getName(), "cities" => $cities];
            }

        }catch (\Exception $exc){}

        return $result;
    }
}
