<?php
/**
 * Created by Ernesto Baez.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-09
 * Time: 19:46
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Author;

class AuthorRepository extends BaseEntityRepository
{
    public function findOrCreate($name)
    {
        $entity = $this->findOneBy(["name" => $name]);

        if (null === $entity)
        {
            $entity = new Author();
            $entity->setName($name);
            $this->save($entity);
        }

        return $entity;
    }
}
