<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-23
 * Time: 00:55
 */

namespace AppBundle\Repository;


class CategoryRepository extends TranslationRepository
{
    public function getIdNameList()
    {
        $result = [];
        foreach ($this->findAll() as $category) {
            $result[] = ["id" => $category->getId(), "name" => $category->getName()];
        }

        return $result;
    }

    protected function getFilter($key)
    {
        return [];
    }

    public function findByFilter($key)
    {
        $_filters = $this->getFilter($key);

        if (!$_filters)
        {
            return $this->findAll();
        }

        $query = $this->createQueryBuilder("e");

        foreach ($_filters as $_filter)
        {
            $query->andWhere($_filter["query"])
                ->setParameter($_filter["param"]["key"], $_filter["param"]["value"]);
        }

        return $query->getQuery()
            ->getResult();
    }

}
