<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-20
 * Time: 12:29
 */

namespace AppBundle\Repository;


class VariableRepository extends BaseEntityRepository
{
    private static $default_locale;

    private static $locales;

    public static function getDefaultLocale()
    {
        return self::$default_locale;
//        return "es";
    }

    /**
     * @param mixed $default_locale
     */
    public static function setDefaultLocale($default_locale)
    {
        self::$default_locale = $default_locale;
    }

    /**
     * @return mixed
     */
    public static function getLocales()
    {
        return self::$locales;
//        return ['es', 'en', 'fr'];
    }

    /**
     * @param mixed $locales
     */
    public static function setLocales($locales)
    {
        self::$locales = $locales;
    }

}