<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-17
 * Time: 11:49
 */

namespace AppBundle\Repository;


use AppBundle\Entity\IBaseEntity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;


class BaseEntityRepository extends EntityRepository
{
    public function resetManager(EntityManagerInterface $em)
    {
        $this->_em = $em;

        if (!$this->_em->isOpen()) {
            $this->_em = $this->_em->create(
                $this->_em->getConnection(),
                $this->_em->getConfiguration()
            );
        }
    }

    public function findPrev(IBaseEntity $entity)
    {
        $result = $this->createQueryBuilder('d')
            ->orderBy('d.id', 'DESC')
            ->where('d.id < :id')
            ->setParameter('id', $entity->getId())
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return count($result) ? $result[0] : Null;
    }

    public function findNext(IBaseEntity $entity)
    {
        $result = $this->createQueryBuilder('d')
            ->orderBy('d.id', 'ASC')
            ->where('d.id > :id')
            ->setParameter('id', $entity->getId())
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return count($result) ? $result[0] : Null;
    }

    /**
     * Persist and commit the request
     * @param $entity
     */
    public function save(IBaseEntity $entity)
    {
        $entity->beforeSave();

        if(!$entity->getId()){
            $this->getEntityManager()->persist($entity);
        }

        $this->getEntityManager()->flush();
    }

    /**
     * Remove and commit the request
     * @param $entity
     */
    public function delete($entity)
    {
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
    }

    public function flush()
    {
        $this->getEntityManager()->flush();
    }

    /**
     * Only persist the entity, you have to call flush function
     * @param $entity
     */
    public function persist($entity)
    {
        $this->getEntityManager()->persist($entity);
    }

    /**
     * Only remove the entity, you have to call flush function
     * @param $entity
     */
    public function remove($entity)
    {
        $this->getEntityManager()->remove($entity);
    }
}
