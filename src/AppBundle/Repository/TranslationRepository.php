<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-18
 * Time: 15:58
 */

namespace AppBundle\Repository;


use AppBundle\Entity\ITranslate;
use Doctrine\ORM\Query;
use Gedmo\Tool\Wrapper\EntityWrapper;
use Gedmo\Translatable\TranslatableListener;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class TranslationRepository extends BaseEntityRepository
{
    /**
     * Current TranslatableListener instance used
     * in EntityManager
     *
     * @var TranslatableListener
     */
    private $listener;

    protected function getIgnoredAttributes(): array
    {
        return [
            'createdBy',
            'updatedBy',
            'updatedAt',
            'createdAt',
            "translations",
            "status",
            "translatableFields"
        ];
    }

    /**
     * Loads all translations with all translatable
     * fields from the given entity
     *
     * @param object $entity Must implement Translatable
     *
     * @return array list of translations in locale groups
     */
    protected function findTranslations($entity)
    {
        $result = array();
        $wrapped = new EntityWrapper($entity, $this->_em);
        if ($wrapped->hasValidIdentifier()) {
            $entityId = $wrapped->getIdentifier();
            $translationClass = $this->getTranslationClass($entity);

            $query = 'trans.object = :entityId';

            $data = $this->_em->createQueryBuilder()
                ->select('trans.content, trans.field, trans.locale')
                ->from($translationClass, 'trans')
                ->where($query)
                ->orderBy('trans.locale')
                ->getQuery()
                ->execute(
                    compact('entityId'),
                    Query::HYDRATE_ARRAY
                );

            if ($data && is_array($data) && count($data)) {
                foreach ($data as $row) {
                    $result[$row['locale']][$row['field']] = $row['content'];
                }
            }
        }

        return $result;
    }

    /**
     * Makes additional translation of $entity $field into $locale
     * using $value
     *
     * @param ITranslate $entity
     * @param string $field
     * @param string $locale
     * @param mixed  $value
     *
     * @throws \Gedmo\Exception\InvalidArgumentException
     *
     * @return static
     */
    public function translate(ITranslate $entity, $field, $locale, $value)
    {
        $translationClass = $this->getTranslationClass($entity);
        $entity->translate(new $translationClass($entity, $locale, $field, $value));

        return $this;
    }

    public function deleteTranslation($entity, $field, $locale)
    {
        $wrapped = new EntityWrapper($entity, $this->_em);
        if ($wrapped->hasValidIdentifier()) {
            $entityId = $wrapped->getIdentifier();

            $translationClass = $this->getTranslationClass($entity);

            return $this->_em->createQueryBuilder()
                ->delete($translationClass, 'trans')
                ->where('trans.object = :entityId',
                    'trans.field = :field',
                    'trans.locale = :locale')
                ->getQuery()
                ->execute(compact('entityId', 'field', 'locale'));
        }

        return null;
    }

    protected function getNormalizerCallbacks($locale)
    {
        return [];
    }

    protected function processVersionTranslation($rawTranslation, $normalizedTranslation){
        return $rawTranslation;
    }

    /**
     * Get all translations versions (included the default locale content). If some content is
     * partially translated the missing content is filled with the default locale's content if
     * param asIs is false, the partial content will be returned otherwise.
     *
     * @param $entity
     * @param array $ignoredAttributes
     * @param bool $asIs
     * @return array
     */
    public function versions(ITranslate $entity, $asIs=False, $ignoredAttributes=[])
    {
        $default_locale = VariableRepository::getDefaultLocale();

        $versions = [];
        $translations = $this->findTranslations($entity);
        $translations[$default_locale] = [];

        $normalizer = new ObjectNormalizer();
        foreach($translations as $locale=>$translation)
        {
            $entity->setLocale($locale);
            $this->_em->refresh($entity);

            // Setting author variable to be normalized
            // $author = $entity->getAuthor();

            if ($ignoredAttr = $ignoredAttributes ? $ignoredAttributes : $this->getIgnoredAttributes())
            {
                $normalizer->setIgnoredAttributes($ignoredAttr);
            }

            if ($callbacks = $this->getNormalizerCallbacks($locale))
            {
                $normalizer->setCallbacks($callbacks);
            }

            $serializer = new Serializer(array($normalizer));
            $normalized = $serializer->normalize($entity);

            if ($asIs && $locale != $default_locale)
            {
                $versions[$locale] = $this->processVersionTranslation($translation, $normalized);
            }
            else
            {
                $versions[$locale] = $normalized;

                if (in_array("note", $entity->getTranslatableFields()) && $locale != $default_locale)
                {
                    $versions[$locale]["note"] = $translation["note"] ?? "";
                }
            }

        }

        return $versions;
    }

    public function getAllVersioned(){
        $locales = VariableRepository::getLocales();

        $versions = [];
        foreach ($this->findAll() as $entity)
        {
            foreach($locales as $locale)
            {
                $entity->setLocale($locale);
                $this->_em->refresh($entity);

                $normalizer = new ObjectNormalizer();
                $normalizer->setIgnoredAttributes($this->getIgnoredAttributes());

                $serializer = new Serializer(array($normalizer));
                $version = $serializer->normalize($entity);
                $versions[$locale][] = ["id" => $version["id"], "name" => $version["name"]];
            }
        }

        return $versions;
    }

    /**
     * Get the currently used TranslatableListener
     *
     * @throws \Gedmo\Exception\RuntimeException - if listener is not found
     *
     * @return TranslatableListener
     */
    protected function getTranslatableListener()
    {
        if (!$this->listener) {
            foreach ($this->_em->getEventManager()->getListeners() as $event => $listeners) {
                foreach ($listeners as $hash => $listener) {
                    if ($listener instanceof TranslatableListener) {
                        return $this->listener = $listener;
                    }
                }
            }

            throw new \Gedmo\Exception\RuntimeException('The translation listener could not be found');
        }

        return $this->listener;
    }

    private function getTranslationClass($entity)
    {
        $wrapped = new EntityWrapper($entity, $this->_em);
        $config = $this
            ->getTranslatableListener()
            ->getConfiguration($this->_em, $wrapped->getMetadata()->name);

        $translationMeta = $this->getClassMetadata(); // table inheritance support

        $translationClass = isset($config['translationClass']) ?
            $config['translationClass'] :
            $translationMeta->rootEntityName;

        return $translationClass;
    }
}
