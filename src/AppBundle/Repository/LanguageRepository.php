<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-29
 * Time: 19:49
 */

namespace AppBundle\Repository;


class LanguageRepository extends BaseEntityRepository
{
    private $list = [
        ["es" => "Bengali", "name" => "Bengali"],
        ["es" => "Danes", "name" => "Dansk"],
        ["es" => "Alemán", "name" => "Deutsch"],
        ["es" => "Inglés", "name" => "English"],
        ["es" => "Español", "name" => "Español"],
        ["es" => "Frances", "name" => "Français"],
        ["es" => "Indi", "name" => "Hindi"],
        ["es" => "Italiano", "name" => "Italiano"],
        ["es" => "Húngaro", "name" => "Magyar"],
        ["es" => "Holandes", "name" => "Nederlands"],
        ["es" => "Noruego", "name" => "Norsk"],
        ["es" => "Polaco", "name" => "Polski"],
        ["es" => "Portugués", "name" => "Português"],
        ["es" => "Punjabi", "name" => "Punjabi"],
        ["es" => "Señas", "name" => "Sign Language"],
        ["es" => "Finlandes", "name" => "Suomi"],
        ["es" => "Sueco", "name" => "Svenska"],
        ["es" => "Philipino", "name" => "Tagalog"],
        ["es" => "Turko", "name" => "Türkçe"],
        ["es" => "Checo", "name" => "Čeština"],
        ["es" => "Griego", "name" => "Ελληνικά"],
        ["es" => "Ruso", "name" => "Русский"],
        ["es" => "Ucraniano", "name" => "українська"],
        ["es" => "Hebreo", "name" => "עברית"],
        ["es" => "Árabe", "name" => "العربية"],
        ["es" => "Tailandés", "name" => "ภาษาไทย"],
        ["es" => "Chino", "name" => "中文"],
        ["es" => "Mandarín", "name" => "國語"],
        ["es" => "Cantonés", "name" => "廣東話"],
        ["es" => "Japones", "name" => "日本語"],
        ["es" => "Coreano", "name" => "한국어"]];

    /**
     * @return array
     */
    public function getEntityData(): array
    {
        return $this->list;
    }
}