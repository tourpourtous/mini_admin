<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-19
 * Time: 12:01
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Nomenclature;


class NomenclatureRepository extends BaseEntityRepository
{
    /**
     * @return array
     */
    public function getFixture(): array
    {
        $entityClass = $this->getEntityName();
        $method = "getFixtures";
        return $entityClass::$method();
    }

    /**
     * @return Nomenclature
     */
    public function getEntityInstance(): Nomenclature
    {
        $entityClass = $this->getEntityName();
        return new $entityClass();
    }

}