<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-03
 * Time: 08:48
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;


/**
 * Class Status
 */
class EntityStatus
{
    const STATUS_COMPLETED = "success";
    const STATUS_INFO = "info";
    const STATUS_PARTIAL = "warning";
    const STATUS_MISSING = "danger";

    const STATUS_NOTE = self::STATUS_INFO;
    const STATUS_TRANSLATED = self::STATUS_COMPLETED;
    const STATUS_NOT_TRANSLATED = self::STATUS_MISSING;

    const ENTITY_STATUS_SUCCESS = 1;
    const ENTITY_STATUS_INFO = 2;
    const ENTITY_STATUS_WARNING = 4;
    const ENTITY_STATUS_DANGER = 8;

    const NOTE_STATUS_SUCCESS = 16;
    const NOTE_STATUS_INFO = 32;
    const NOTE_STATUS_WARNING = 64;
    const NOTE_STATUS_DANGER = 128;

    const ES_STATUS_SUCCESS = self::ENTITY_STATUS_SUCCESS;
    const ES_STATUS_INFO = self::ENTITY_STATUS_INFO;
    const ES_STATUS_WARNING = self::ENTITY_STATUS_WARNING;
    const ES_STATUS_DANGER = self::ENTITY_STATUS_DANGER;

    const EN_STATUS_SUCCESS = self::NOTE_STATUS_SUCCESS;
    const EN_STATUS_INFO = self::NOTE_STATUS_INFO;
    const EN_STATUS_WARNING = self::NOTE_STATUS_WARNING;
    const EN_STATUS_DANGER = self::NOTE_STATUS_DANGER;

    const FR_STATUS_SUCCESS = 256;
    const FR_STATUS_INFO = 512;
    const FR_STATUS_WARNING = 1024;
    const FR_STATUS_DANGER = 2048;

    const FIXTURE = [
        EntityStatus::STATUS_COMPLETED,
        EntityStatus::STATUS_INFO,
        EntityStatus::STATUS_PARTIAL,
        EntityStatus::STATUS_MISSING
    ];

}