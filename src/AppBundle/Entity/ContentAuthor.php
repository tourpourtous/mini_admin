<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-17
 * Time: 15:13
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ContentAuthor
 * @package AppBundle\Entity
 * @ORM\MappedSuperclass
 */
abstract class ContentAuthor extends BaseEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Author", cascade={"persist"})
     * @ORM\JoinColumn(name="author", referencedColumnName="id")
     */
    private $author;

    protected $content;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $locale;

    public function __construct($content, Author $author, $locale)
    {
        $this->setContent($content);
        $this->setAuthor($author);
        $this->setLocale($locale);
    }

    public function getLocale(){
        return $this->locale;
    }

    public function setLocale($value){
        $this->locale = $value;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($value)
    {
        $this->author = $value;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($value)
    {
        $this->content = $value;
    }
}
