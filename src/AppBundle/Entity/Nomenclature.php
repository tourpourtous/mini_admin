<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-17
 * Time: 10:36
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Asserts;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * Class Nomenclature
 * @package AppBundle\Entity
 * @ORM\MappedSuperclass(repositoryClass="AppBundle\Repository\NomenclatureRepository")
 * @UniqueEntity("code")
 */
abstract class Nomenclature extends BaseEntity
{
    const FIXTURE = [];

    /**
     * @ORM\Column(type="string", length=10, unique=true)
     * @Asserts\NotBlank(message = "input.not_blank")
     * @Asserts\NotNull()
     */
    private $code;

    public function __construct($code = "")
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }
}
