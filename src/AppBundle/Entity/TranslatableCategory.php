<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-17
 * Time: 10:36
 */

namespace AppBundle\Entity;


use AppBundle\Entity\EntityTraits\TTranslatableName;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class TranslatableCategory
 * @package AppBundle\Entity
 * @ORM\MappedSuperclass
 */
abstract class TranslatableCategory extends BaseEntity
{
    use TTranslatableName;
}
