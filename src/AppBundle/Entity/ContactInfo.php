<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-25
 * Time: 17:08
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity
 * @ORM\Table(name="contact_info",
 * uniqueConstraints={@ORM\UniqueConstraint(name="contact_unique_idx", columns={
 *         "email", "cell_number", "land_number"
 *     })})
 */
class ContactInfo extends BaseEntity
{
    /**
     * @ORM\Column(type="text", length=140, nullable=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="text", length=20, nullable=true)
     */
    protected $cellNumber;

    /**
     * @ORM\Column(type="text", length=20, nullable=true)
     */
    protected $landNumber;

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getCellNumber()
    {
        return $this->cellNumber;
    }

    /**
     * @param mixed $cellNumber
     */
    public function setCellNumber($cellNumber)
    {
        $this->cellNumber = $cellNumber;
    }

    /**
     * @return mixed
     */
    public function getLandNumber()
    {
        return $this->landNumber;
    }

    /**
     * @param mixed $landNumber
     */
    public function setLandNumber($landNumber)
    {
        $this->landNumber = $landNumber;
    }
}
