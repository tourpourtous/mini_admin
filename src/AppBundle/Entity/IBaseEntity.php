<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-25
 * Time: 14:22
 */

namespace AppBundle\Entity;


interface IBaseEntity
{
    function getId();

    function beforeSave();
}
