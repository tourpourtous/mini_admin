<?php
/**
 * Created by Ernesto Baez.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-09
 * Time: 19:01
 */

namespace AppBundle\Entity;


use AppBundle\Entity\EntityTraits\TExtra;
use AppBundle\Entity\EntityTraits\TLocation;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Asserts;


/**
 * @ORM\Table(name="address")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AddressRepository")
 */
class Address extends BaseEntity
{
    const ELEVATOR = 1;
    const HALF_FLOOR = 2;

    use TLocation;
    use TExtra;

    /**
     * @ORM\Column(type="string", length=128)
     * @Asserts\NotBlank(message = "input.not_blank")
     * @Asserts\NotNull()
     */
    private $address1;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $address2;

    /**
     * @ORM\Column(type="string", length=128)
     * @Asserts\NotBlank(message = "input.not_blank")
     * @Asserts\NotNull()
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=128)
     * @Asserts\NotBlank(message = "input.not_blank")
     * @Asserts\NotNull()
     */
    public $state;

    /**
     * @ORM\Column(type="string", length=128)
     * @Asserts\NotBlank(message = "input.not_blank")
     * @Asserts\NotNull()
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     */
    private $zip;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $floor;

    /**
     * @return mixed
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * @param mixed $address1
     */
    public function setAddress1($address1)
    {
        $this->address1 = trim($address1);
    }

    /**
     * @return mixed
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @param mixed $address2
     */
    public function setAddress2($address2)
    {
        $this->address2 = trim($address2);
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = trim($city);
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = trim($state);
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = trim($country);
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param mixed $zip
     */
    public function setZip($zip)
    {
        $this->zip = trim($zip);
    }

    /**
     * @return mixed
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * @param mixed $value
     */
    public function setFloor($value)
    {
        $this->floor = $value;
    }

    /**
     * @return mixed
     */
    public function getElevator()
    {
        return boolval($this->getExtra(Address::ELEVATOR));
    }

    /**
     * @param bool $value
     */
    public function setElevator($value)
    {
        $this->setExtra(Address::ELEVATOR, $value);
    }

    /**
     * @return mixed
     */
    public function getHalfFloor()
    {
        return boolval($this->getExtra(Address::HALF_FLOOR));
    }

    /**
     * @param bool $value
     */
    public function setHalfFloor($value)
    {
        $this->setExtra(Address::HALF_FLOOR, $value);
    }
}
