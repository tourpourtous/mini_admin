<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-25
 * Time: 16:42
 */

namespace AppBundle\Entity\EntityTraits;


use Doctrine\ORM\Mapping as ORM;


trait TAddress
{
    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Address", cascade={"persist"})
     */
    private $address;

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }
}
