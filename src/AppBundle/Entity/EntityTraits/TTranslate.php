<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-19
 * Time: 14:10
 */

namespace AppBundle\Entity\EntityTraits;


use AppBundle\Entity\EntityStatus;
use AppBundle\Repository\VariableRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;
use Gedmo\Mapping\Annotation as Gedmo;


trait TTranslate
{
    use TEntityStatus;

    protected $translations;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     * and it is not necessary because globally locale can be set in listener
     */
    protected $locale;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    protected function getStatusMapping()
    {
        foreach (VariableRepository::getLocales() as $locale) {
            yield $locale => strtoupper($locale);
        }
    }

    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    protected function getLocale()
    {
        return $this->locale;
    }

    public function getTranslations()
    {
        return $this->translations ?? [];
    }

    /**
     * @param AbstractPersonalTranslation $pt
     * @return AbstractPersonalTranslation|null
     */
    protected function findTranslation(AbstractPersonalTranslation $pt)
    {
        $predicate = function($key, $element) use ($pt) {
            return $element->getLocale() == $pt->getLocale() && $element->getField() == $pt->getField();
        };

        foreach ($this->getTranslations() as $key => $element) {
            if ($predicate($key, $element)) {
                return $element;
            }
        }

        return null;
    }

    public function translate(AbstractPersonalTranslation $pt)
    {
        $translation = $this->findTranslation($pt);
        if (!$translation) {
            $this->translations->add($pt);
        }else
        {
            $translation->setContent($pt->getContent());
        }

        $pt->setObject($this);
    }

    /**
     * @param string $entityFieldVerifications the function interface should be function(&$status){...}
     */
    protected function calculateStatus($entityFieldVerifications="")
    {
        $default_locale = VariableRepository::getDefaultLocale();
        $locales = VariableRepository::getLocales();

        $status[$default_locale] = EntityStatus::ES_STATUS_SUCCESS;

        if (method_exists($this, 'getNote') && $this->getNote())
        {
            $status[$default_locale] = EntityStatus::ES_STATUS_INFO;
        }

        if (!empty($entityFieldVerifications))
        {
            $entityFieldVerifications($status);
        }

        $translatableFields = $this->getTranslatableFields();
        $translationStatus = [];
        $has_note = in_array("note", $translatableFields);
        foreach ($this->getTranslations() as $translation)
        {
            $locale = $translation->getLocale();
            if (in_array($locale, $locales) && in_array($translation->getField(), $translatableFields) && $translation->getContent())
            {
                $capitalizedLocale = strtoupper($locale);
                $L_STATUS_SUCCESS = constant(EntityStatus::class."::".$capitalizedLocale."_STATUS_SUCCESS");
                $L_STATUS_INFO = constant(EntityStatus::class."::".$capitalizedLocale."_STATUS_INFO");
                $L_STATUS_WARNING = constant(EntityStatus::class."::".$capitalizedLocale."_STATUS_WARNING");

                $translationStatus[$locale] = isset($translationStatus[$locale]) ? $translationStatus[$locale] + 1 : 1;

                if ($translation->getField() == "note")
                {
                    $status[$locale] = $translation->getContent() ? $L_STATUS_INFO : $status[$locale];
                }

                $note_count = $has_note ? count($translatableFields) - 1 : count($translatableFields);
                $partialStatus = $translationStatus[$locale] == $note_count ? $L_STATUS_SUCCESS : $L_STATUS_WARNING;
                $status[$locale] = isset($status[$locale]) && $status[$locale] == $L_STATUS_INFO ? $L_STATUS_INFO : $partialStatus;
            }
        }

        $this->setStatus($status);
    }

    /**
     * @return bool Return if the entity general status is ok (often used know if a content can be published)
     */
    public function isOk()
    {
        $const = 0;
        foreach ($this->getStatusMapping() as $key => $constant)
        {
            $const |= constant(EntityStatus::class."::".$constant."_STATUS_SUCCESS");
        }

        return ($this->getRawStatus() & $const) == $const;
    }
}
