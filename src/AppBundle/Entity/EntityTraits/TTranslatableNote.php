<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-31
 * Time: 10:38
 */

namespace AppBundle\Entity\EntityTraits;


use Gedmo\Mapping\Annotation as Gedmo;


trait TTranslatableNote
{
    use TNote;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     */
    protected $note = "";
}
