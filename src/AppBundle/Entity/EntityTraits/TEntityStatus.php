<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-31
 * Time: 10:38
 */

namespace AppBundle\Entity\EntityTraits;


use AppBundle\Entity\EntityStatus;
use Doctrine\ORM\Mapping as ORM;


trait TEntityStatus
{
    /**
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $status = 0;

    protected abstract function getStatusMapping();

    /**
     * Function defined in child class since is the one who knows how is calculated the status.
     *
     * @return void
     */
    protected abstract function calculateStatus();

    protected function getRawStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        $status = [];
        foreach ($this->getStatusMapping() as $key => $constant)
        {
            $constants =[
                constant(EntityStatus::class."::".$constant."_STATUS_SUCCESS") => EntityStatus::STATUS_COMPLETED,
                constant(EntityStatus::class."::".$constant."_STATUS_INFO") => EntityStatus::STATUS_INFO,
                constant(EntityStatus::class."::".$constant."_STATUS_WARNING") => EntityStatus::STATUS_PARTIAL,
                constant(EntityStatus::class."::".$constant."_STATUS_DANGER") => EntityStatus::STATUS_MISSING
            ];

            $status[$key] = EntityStatus::STATUS_MISSING;
            foreach ($constants as $bit => $name) {
                if ($this->status & $bit)
                {
                    $status[$key] = $name;
                    break;
                }
            }
        }

        return $status;
    }

    /**
     * @param array $value
     */
    protected function setStatus($value)
    {
        $this->status = 0;
        foreach ($value as $key => $constant)
        {
                $this->status |= $constant;
        }
    }

    public function beforeSave()
    {
        $this->calculateStatus();
    }

    /**
     * @return bool Return if the entity general status is ok (often used know if a content can be published)
     */
    public abstract function isOk();
}
