<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-31
 * Time: 10:38
 */

namespace AppBundle\Entity\EntityTraits;


use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Asserts;


trait TTranslatableDescription
{
    use TDescription;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text")
     * @Asserts\NotBlank(message = "input.not_blank")
     */
    protected $description="";
}
