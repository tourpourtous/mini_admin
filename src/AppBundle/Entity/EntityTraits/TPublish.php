<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-29
 * Time: 19:59
 */

namespace AppBundle\Entity\EntityTraits;


use Doctrine\ORM\Mapping as ORM;


trait TPublish
{
    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $published=false;

    /**
     * @return bool
     */
    public function getPublished(): bool
    {
        return $this->published;
    }

    /**
     * @param bool $published
     */
    public function setPublished($published)
    {
        $this->published = boolval($published);
    }

}