<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-18
 * Time: 16:52
 */

namespace AppBundle\Entity\EntityTraits;


trait TCategory
{
    protected $category;

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($value)
    {
        $this->category = $value;
    }
}