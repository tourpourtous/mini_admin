<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-25
 * Time: 17:07
 */

namespace AppBundle\Entity\EntityTraits;


use AppBundle\Entity\ContactInfo;


trait TContactInfo
{
    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\ContactInfo", cascade={"persist"})
     */
    private $contact;

    /**
     * @return ContactInfo
     */
    public function getContact(): ContactInfo
    {
        return $this->contact;
    }

    /**
     * @param ContactInfo $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }
}
