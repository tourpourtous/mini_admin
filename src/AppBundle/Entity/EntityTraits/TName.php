<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-31
 * Time: 10:38
 */

namespace AppBundle\Entity\EntityTraits;


use Symfony\Component\Validator\Constraints as Asserts;


trait TName
{
    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Asserts\NotBlank(message = "input.not_blank")
     * @Asserts\NotNull()
     */
    protected $name="";

    public function getName(){
        return $this->name;
    }

    public function setName($value){
        $this->name = $value;
    }
}
