<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-31
 * Time: 10:38
 */

namespace AppBundle\Entity\EntityTraits;


use AppBundle\Entity\Author;


trait TAuthor
{
    protected $authors;

    protected $author;

    protected abstract function getContentAuthorInstance($author, $language);

    public function getAuthors()
    {
        return $this->authors;
    }

    public function setAuthors($content_author)
    {
        $this->authors[] = $content_author;
    }

    public function setAuthor(Author $author, $language)
    {
        $element = null;

        foreach ($this->getAuthors() as $key => $_element) {
            if ($_element->getLocale() == $language) {
                $element = $_element;
                break;
            }
        }

        $content_author = $this->getContentAuthorInstance($author, $language);

        if (!$element) {
            $content_author->setLocale($language);
            $this->setAuthors($content_author);
        }else{
            $element->setAuthor($author);
        }
    }

    public function unsetAuthor($language)
    {
        foreach ($this->getAuthors() as $key => $_element) {
            if ($_element->getLocale() == $language) {
                $this->getAuthors()->removeElement($_element);
                return $_element;
                break;
            }
        }

        return null;
    }

    public function getAuthor()
    {
        $content_authors = $this->getAuthors();
        foreach($content_authors as $content_author)
        {
            if($content_author->getLocale() == $this->getLocale())
            {
                $this->author = $content_author->getAuthor();
                return $this->author;
            }
        }

        return null;
    }

}
