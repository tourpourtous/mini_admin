<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-31
 * Time: 10:38
 */

namespace AppBundle\Entity\EntityTraits;


trait TDescription
{
    /**
     * @ORM\Column(type="text")
     * @Asserts\NotBlank(message = "input.not_blank")
     */
    protected $description="";

    public function getDescription(){
        return $this->description;
    }

    public function setDescription($value){
        $this->description = $value;
    }
}
