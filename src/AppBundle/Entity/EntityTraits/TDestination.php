<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-18
 * Time: 16:52
 */

namespace AppBundle\Entity\EntityTraits;


use Doctrine\ORM\Mapping as ORM;


trait TDestination
{
    /**
     * @ORM\ManyToOne(targetEntity="ContentBundle\Entity\Destination")
     * @ORM\JoinColumn(name="destination_id", referencedColumnName="id", nullable=true)
     */
    private $destination;

    public function getDestination()
    {
        return $this->destination;
    }

    public function setDestination($value)
    {
        $this->destination = $value;
    }
}