<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-31
 * Time: 10:38
 */

namespace AppBundle\Entity\EntityTraits;


trait TNote
{
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $note = "";

    public function getNote(){
        return $this->note;
    }

    public function setNote($value){
        $this->note = trim($value);
    }
}
