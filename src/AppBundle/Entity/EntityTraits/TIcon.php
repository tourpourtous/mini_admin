<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-29
 * Time: 19:59
 */

namespace AppBundle\Entity\EntityTraits;


use Doctrine\ORM\Mapping as ORM;


trait TIcon
{
    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $icon="";

    /**
     * @return null|string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param null|string $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

}