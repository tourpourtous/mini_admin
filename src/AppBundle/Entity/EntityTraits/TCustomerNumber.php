<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-25
 * Time: 21:15
 */

namespace AppBundle\Entity\EntityTraits;


use Symfony\Component\Validator\Constraints as Asserts;


trait TCustomerNumber
{
    /**
     * @ORM\Column(type="string", length=10, unique=true, nullable=true)
     * @Asserts\NotBlank(message = "input.not_blank")
     */
    private $customerNumber;

    /**
     * @return mixed
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }

    /**
     * @param mixed $value
     */
    public function setCustomerNumber($value)
    {
        $this->customerNumber = trim($value);
    }
}
