<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-31
 * Time: 10:38
 */

namespace AppBundle\Entity\EntityTraits;


trait TLocation
{
    /**
     * @ORM\Column(type="decimal", scale=8)
     * @Asserts\NotBlank(message = "input.not_blank")
     * @Asserts\NotNull()
     */
    private $lng = 0;

    /**
     * @ORM\Column(type="decimal", scale=8)
     * @Asserts\NotBlank(message = "input.not_blank")
     * @Asserts\NotNull()
     */
    private $lat = 0;

    public function getCoordinates(){
        return ["lng" => $this->lng, "lat" => $this->lat];
    }

    public function setCoordinates($value){
        $this->lng = $value["lng"];
        $this->lat = $value["lat"];
    }

    public function hasDefaultCoordinates()
    {
        return $this->lng == -82.359591 && $this->lat == 23.135285;
    }
}
