<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-25
 * Time: 17:32
 */

namespace AppBundle\Entity\EntityTraits;


trait TExtra
{
    /**
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $extra = 0;

    public function getRawExtra()
    {
        return $this->extra;
    }

    protected function getExtra($CONST)
    {
        return $this->extra & $CONST;
    }

    public function setExtra($CONST, $add=true)
    {
        if($add)
        {
            $this->extra |= $CONST;
        }
        else
        {
            $this->extra ^= $CONST;
        }
    }
}
