<?php
/**
 * Created by Ernesto Baez.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-09
 * Time: 19:01
 */

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Table(name="state",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="state_unique_idx", columns={
 *         "name", "country_id"
 *     }),
 *     @ORM\UniqueConstraint(name="state_code_unique_idx", columns={
 *         "code", "country_id"
 *     })}
 *)
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StateRepository")
 */
class State extends Location
{
    /**
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $country;

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }
}
