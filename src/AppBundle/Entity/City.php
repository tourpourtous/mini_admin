<?php
/**
 * Created by Ernesto Baez.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-09
 * Time: 19:01
 */

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Table(name="city",
 *     uniqueConstraints={
 *     @ORM\UniqueConstraint(name="city_unique_idx", columns={
 *         "name", "state_id"
 *     }),
 *     @ORM\UniqueConstraint(name="city_code_unique_idx", columns={
 *         "code", "state_id"
 *     })}
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseEntityRepository")
 */
class City extends Location
{
    /**
     * @ORM\ManyToOne(targetEntity="State")
     * @ORM\JoinColumn(name="state_id", referencedColumnName="id")
     */
    private $state;

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }
}
