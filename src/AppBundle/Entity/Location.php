<?php
/**
 * Created by Ernesto Baez.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-09
 * Time: 19:01
 */

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Asserts;


/**
 * @ORM\MappedSuperclass
 */
abstract class Location extends BaseEntity
{
    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=125)
     * @Asserts\NotBlank(message = "input.not_blank")
     * @Asserts\NotNull()
     */
    private $name="";

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $code;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }
}
