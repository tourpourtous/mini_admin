<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-25
 * Time: 10:38
 */

namespace AppBundle\Entity;


use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class TranslationClass
 * @package AppBundle\Entity
 * @ORM\MappedSuperclass
 */
abstract class BaseTranslation extends AbstractPersonalTranslation
{
    /**
     * Convenient constructor
     *
     * @param $object
     * @param string $locale
     * @param string $field
     * @param string $value
     */
    public function __construct($object, $locale, $field, $value)
    {
        $this->setObject($object);
        $this->setLocale($locale);
        $this->setField($field);
        $this->setContent($value);
    }
}
