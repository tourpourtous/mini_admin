<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-19
 * Time: 17:09
 */

namespace AppBundle\Entity;


use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;

interface ITranslate
{
    function setLocale($locale);

    function getTranslations();

    function getTranslatableFields();

    function translate(AbstractPersonalTranslation $translation);

}