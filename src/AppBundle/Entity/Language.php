<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-27
 * Time: 13:37
 */

namespace AppBundle\Entity;


use AppBundle\Entity\EntityTraits\TName;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Language
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LanguageRepository")
 * @ORM\Table(name="language")
 */
class Language extends BaseEntity
{
    use TName;

    /**
     * @ORM\Column(type="text")
     */
    private $es_name;

    /**
     * @ORM\Column(type="string", length=20, unique=true)
     * @Asserts\NotBlank(message = "input.not_blank")
     * @Asserts\NotNull()
     */
    protected $name="";

    /**
     * @return mixed
     */
    public function getEsName()
    {
        return $this->es_name;
    }

    /**
     * @param mixed $es_name
     */
    public function setEsName($es_name)
    {
        $this->es_name = $es_name;
    }
}