<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-17
 * Time: 10:36
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\EntityTraits\TBlame;
use Gedmo\Timestampable\Traits\TimestampableEntity;


/**
 * Class BaseEntity
 * @package AppBundle\Entity
 * @ORM\MappedSuperclass(repositoryClass="AppBundle\Repository\BaseEntityRepository")
 */
abstract class BaseEntity implements IBaseEntity
{

    use TBlame;

    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    protected $updatedAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    public function getId(){
        return $this->id;
    }

    public function beforeSave(){}
}
