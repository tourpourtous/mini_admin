<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-17
 * Time: 10:36
 */

namespace AppBundle\Entity;


use AppBundle\Entity\EntityTraits\TIcon;
use AppBundle\Entity\EntityTraits\TTranslatableName;
use AppBundle\Entity\EntityTraits\TTranslate;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Category
 * @package AppBundle\Entity
 * @ORM\MappedSuperclass(repositoryClass="AppBundle\Repository\CategoryRepository")
 */
abstract class Category extends BaseEntity implements ITranslate
{
    use TTranslatableName;
    use TIcon;
    use TTranslate;

    public function getTranslatableFields()
    {
        return ["name"];
    }

    public function useIcon()
    {
        return false;
    }
}
