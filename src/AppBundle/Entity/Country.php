<?php
/**
 * Created by Ernesto Baez.
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-08-09
 * Time: 19:01
 */

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Table(name="country")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseEntityRepository")
 */
class Country extends Location {}
