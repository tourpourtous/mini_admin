<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-29
 * Time: 21:34
 */

namespace AppBundle\Twig;

class AppExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            new \Twig_Function('set_class', array($this, 'SetClass')),
        );
    }

    /**
     * Returns $classIfTrue if expressions is true, $classIfFalse is returned otherwise
     *
     * @param $expression
     * @param $classIfTrue
     * @param $classIfFalse
     * @return string
     */
    public function setClass($expression, $classIfTrue, $classIfFalse)
    {
        return $expression ? $classIfTrue : $classIfFalse;
    }
}