<?php

namespace AppBundle\Command;

use AppBundle\Entity\EntityStatus;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UserBundle\Entity\User;

class SetUserStatusCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:set_user_status')
            ->setDescription('Set user status value.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Start',
            '============',
        ]);

        $repository = $this->getContainer()
            ->get("doctrine.orm.entity_manager")
            ->getRepository(User::class);

        $users = $repository->findAll();

        foreach ($users as $user)
        {
            $repository->save($user);
        }

        $output->writeln([
            'Done'
        ]);

    }
}
