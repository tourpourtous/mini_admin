<?php

namespace AppBundle\Command;


use AccommodationBundle\Entity\BathroomType;
use AccommodationBundle\Entity\BedType;
use AccommodationBundle\Entity\AccommodationOption;
use AccommodationBundle\Entity\AccommodationService;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class ImportAccommodationDataCommand extends ContainerAwareCommand
{
    use TWebCommand;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:import_accommodation_data')
            ->setDescription('Import users from csv file');
    }

    public function runCommand($container)
    {
        $this->setContainer($container);
        $this->execute(new StringInput(""), new NullOutput());
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Start',
            '============',
        ]);

        $services = [
            [
                "icon" => "tpt-icon-breakfast",
                "data" => [
                    "es" => "Desayuno",
                    "en" => "Breakfast",
                    "fr" => "Déjeuner"
                ]
            ],
            [
                "icon" => "tpt-icon-lunch",
                "data" => [
                    "es" => "Almuerzo",
                    "en" => "Lunch",
                    "fr" => "Repas du midi"
                ]
            ],
            [
                "icon" => "tpt-icon-diner",
                "data" => [
                    "es" => "Cena",
                    "en" => "Diner",
                    "fr" => "Repas du soire"
                ]
            ],
            [
                "icon" => "tpt-icon-laundry",
                "data" => [
                    "es" => "Lavandería",
                    "en" => "Laundry",
                    "fr" => "Lessive"
                ]
            ],
            [
                "icon" => "tpt-icon-kitchen",
                "data" => [
                    "es" => "Uso de cocina",
                    "en" => "Use of the kitchen",
                    "fr" => "Utilisation de la cuisine"
                ]
            ],
            [
                "icon" => "tpt-icon-internet",
                "data" => [
                    "es" => "Internet",
                    "en" => "Internet",
                    "fr" => "Internet"
                ]
            ],
            [
                "icon" => "tpt-icon-wifi",
                "data" => [
                    "es" => "Wifi",
                    "en" => "Wifi",
                    "fr" => "Wifi"
                ]
            ],
            [
                "icon" => "tpt-icon-clé",
                "data" => [
                    "es" => "Entrega de llave",
                    "en" => "Access avec clé",
                    "fr" => "Access with key"
                ]
            ],
            [
                "icon" => "tpt-icon-bicycle",
                "data" => [
                    "es" => "Bicicleta",
                    "en" => "Bicycle",
                    "fr" => "Vélo"
                ]
            ],
            [
                "icon" => "tpt-icon-parking-auto",
                "data" => [
                    "es" => "Parqueo auto",
                    "en" => "Parking auto",
                    "fr" => "Parking auto"
                ]
            ],
            [
                "icon" => "tpt-icon-parking-bike",
                "data" => [
                    "es" => "Parqueo bici",
                    "en" => "Parking bike",
                    "fr" => "Parking vélo"
                ]
            ],
            [
                "icon" => "tpt-icon-bar-coctel",
                "data" => [
                    "es" => "Bar / Cóctel",
                    "en" => "Bar / Cóctel",
                    "fr" => "Bar / Cóctel"
                ]
            ],
            [
                "icon" => "tpt-icon-pool",
                "data" => [
                    "es" => "Piscina",
                    "en" => "Swimming pool",
                    "fr" => "Piscine"
                ]
            ],
            [
                "icon" => "tpt-icon-jacuzzi",
                "data" => [
                    "es" => "Jacuzzi",
                    "en" => "Jacuzzi",
                    "fr" => "Jacuzzi"
                ]
            ]
        ];

        $accommodation_conditions = [
            [
                "es" => "Espacios interiores",
                "en" => "Interior spaces",
                "fr" => "Espaces intérieurs"
            ],
            [
                "es" => "Espacios exteriores",
                "en" => "Outdoor Spaces",
                "fr" => "Espaces extérieurs"
            ],
            [
                "es" => "Área de fumadores",
                "en" => "Smoking area",
                "fr" => "Zone fumeur"
            ],
            [
                "es" => "Anfitrión presente",
                "en" => "Host in place",
                "fr" => "Hôte présente"
            ],
            [
                "es" => "Teléfono",
                "en" => "Phone",
                "fr" => "Téléfone"
            ],
            [
                "es" => "110 / 220 Volts",
                "en" => "110 / 220 Volts",
                "fr" => "110 / 220 Volts"
            ]
        ];

        $accommodation_safety = [
            [
                "es" => "Extintor",
                "en" => "Extinguisher",
                "fr" => "Extinteur"
            ],
            [
                "es" => "Kit primeros auxilios",
                "en" => "First aid kit",
                "fr" => "Trousse de premiers secours"
            ],
            [
                "es" => "Detector de humo",
                "en" => "Smoke detector",
                "fr" => "Détecteur de fumée"
            ]
        ];

        $accommodation_allows = [
            [
                "es" => "Niños menores de 5 años",
                "en" => "Children under 5 years old",
                "fr" => "Enfants de moins de 5 ans"
            ],
            [
                "es" => "Niños de 5 a 12 años",
                "en" => "Children between 5 and 12 years old",
                "fr" => "Enfants de 5 à 12 ans"
            ],
            [
                "es" => "Fiestas / cumpleaños",
                "en" => "Parties / Birthdays",
                "fr" => "Parties / anniversaires"
            ],
            [
                "es" => "Fumadores",
                "en" => "Smokers",
                "fr" => "Fumeurs"
            ],
            [
                "es" => "Discapacitados",
                "en" => "Handicapped",
                "fr" => "Handicapé"
            ],
            [
                "es" => "Mascotas",
                "en" => "Pets",
                "fr" => "Animaux domestiques"
            ]
        ];

        $room_conditions = [
            [
                "es" => "Vista al exterior",
                "en" => "View to outside",
                "fr" => "Vue a l'extérieure"
            ],
            [
                "es" => "Jacuzzi",
                "en" => "Jacuzzi",
                "fr" => "Jacuzzi"
            ],
            [
                "es" => "Agua caliente",
                "en" => "Hot water",
                "fr" => "Eau chaude"
            ]
        ];

        $room_safety = [
            [
                "es" => "Cuartos con cerradura",
                "en" => "Room with lock",
                "fr" => "Chambre avec serrure"
            ],
            [
                "es" => "Baño con cerradura",
                "en" => "Bathroom with lock",
                "fr" => "Salle de bain avec serrure"
            ],
            [
                "es" => "Caja fuerte",
                "en" => "Safebox",
                "fr" => "Coffre-fort"
            ],
            [
                "es" => "Detector de humo",
                "en" => "Smoke detector",
                "fr" => "Détecteur de fumée"
            ]
        ];

        $room_amenities = [
            [
                "es" => "Tv",
                "en" => "Tv",
                "fr" => "Tv"
            ],
            [
                "es" => "Aire acondicionado",
                "en" => "Air conditioner",
                "fr" => "Climatisation"
            ],
            [
                "es" => "Cocina",
                "en" => "Kitchen",
                "fr" => "Cuicine"
            ],
            [
                "es" => "Refrigerador",
                "en" => "Fridge",
                "fr" => "Frigo"
            ],
            [
                "en" => "Minibar",
                "es" => "Minibar",
                "fr" => "Minibar"
            ],
            [
                "es" => "Estéreos",
                "en" => "Stereo",
                "fr" => "Stereo"
            ],
            [
                "es" => "Plancha",
                "en" => "Iron",
                "fr" => "Fer à repasser"
            ],
            [
                "es" => "Lavadora",
                "en" => "Washing machine",
                "fr" => "Laveuse"
            ],
            [
                "es" => "Secadora",
                "en" => "Dryer",
                "fr" => "Sécheuse"
            ],
            [
                "es" => "Jabón/gel de baño",
                "en" => "Soap / Gel",
                "fr" => "Savon / Gel"
            ],
            [
                "es" => "Champú",
                "en" => "Shampoo",
                "fr" => "Shampooing"
            ],
            [
                "es" => "Toalla",
                "en" => "Towel",
                "fr" => "Serviette"
            ],
            [
                "es" => "Secador de pelo",
                "en" => "Hair dryer",
                "fr" => "Sèche-cheveux"
            ]
        ];

        $options = [
            [
                "group" => AccommodationOption::GROUP_CONDITIONS,
                "container" => true,
                "data" => $accommodation_conditions,
            ],
            [
                "group" => AccommodationOption::GROUP_SAFETY,
                "container" => true,
                "data" => $accommodation_safety,
            ],
            [
                "group" => AccommodationOption::GROUP_ALLOWS,
                "container" => true,
                "data" => $accommodation_allows,
            ],
            [
                "group" => AccommodationOption::GROUP_CONDITIONS,
                "container" => false,
                "data" => $room_conditions,
            ],
            [
                "group" => AccommodationOption::GROUP_SAFETY,
                "container" => false,
                "data" => $room_safety,
            ],
            [
                "group" => AccommodationOption::GROUP_AMENITIES,
                "container" => false,
                "data" => $room_amenities,
            ]
        ];

        $beds = [
            [
                "icon" => "tpt-icon-bed-personal",
                "data" =>
                    [
                        "es" => "Personal",
                        "en" => "Personal",
                        "fr" => "Personnel"
                    ]
            ],
            [
                "icon" => "tpt-icon-bed-double",
                "data" =>
                    [
                        "es" => "Doble",
                        "en" => "Double",
                        "fr" => "Double"
                    ]
            ],
            [
                "icon" => "tpt-icon-bed-queen",
                "data" =>
                    [
                        "es" => "Queen",
                        "en" => "Queen",
                        "fr" => "Queen"
                    ]
            ],
            [
                "icon" => "tpt-icon-bed-king",
                "data" =>
                    [
                        "es" => "King",
                        "en" => "King",
                        "fr" => "King"
                    ]
            ],
            [
                "icon" => "tpt-icon-bed-bunk",
                "data" =>
                    [
                        "es" => "Litera",
                        "en" => "Bunk",
                        "fr" => "Litière"
                    ]
            ],
            [
                "icon" => "tpt-icon-bed-sofa",
                "data" =>
                    [
                        "es" => "Sofá cama",
                        "en" => "Sofa-bed",
                        "fr" => "Sofa-lit"
                    ]
            ],
            [
                "icon" => "tpt-icon-bed-cradle",
                "data" =>
                    [
                        "es" => "Cuna",
                        "en" => "Cradle",
                        "fr" => "Cradle"
                    ]
            ]
        ];

        $baths = [
            [
                "icon" => "tpt-icon-bath-private",
                "data" =>
                    [
                        "es" => "Privado",
                        "en" => "Private",
                        "fr" => "Privée"
                    ]
            ],
            [
                "icon" => "tpt-icon-bath-shared",
                "data" =>
                    [
                        "es" => "Compartido",
                        "en" => "Shared",
                        "fr" => "Partagée"
                    ]
            ]
        ];

        $default_locale = $this->getContainer()->getParameter("locale");

        $repository = $this->getContainer()
            ->get("doctrine.orm.entity_manager")
            ->getRepository(AccommodationOption::class);

        foreach ($options as $option)
        {
            foreach ($option["data"] as $data) {
                $entity = new AccommodationOption();
                $entity->setGroups($option["group"], $option["container"]);

                foreach ($data as $locale => $text) {
                    if ($locale == $default_locale) {
                        $entity->setName($text);
                    } else {
                        $repository->translate($entity, "name", $locale, $text);
                    }
                }
                $repository->save($entity);
            }
        }

        $this->create_entities(AccommodationService::class, $services);

        $this->create_entities(BedType::class, $beds);

        $this->create_entities(BathroomType::class, $baths);

        $output->writeln([
            'Done'
        ]);
    }

    private function create_entities($class, $fixture)
    {
        $default_locale = $this->getContainer()->getParameter("locale");

        $repository = $this->getContainer()
            ->get("doctrine.orm.entity_manager")
            ->getRepository($class);

        foreach ($fixture as $item)
        {
            if (!($entity = $repository->findOneBy(["name" => $item["data"][$default_locale]])))
            {
                $entity = new $class();
            }

            $entity->setIcon($item["icon"]);

            foreach ($item["data"] as $locale => $text) {
                if ($locale == $default_locale)
                {
                    $entity->setName($text);
                }
                else
                {
                    $repository->translate($entity, "name", $locale, $text);
                }
            }

            $repository->save($entity);
        }
    }
}
