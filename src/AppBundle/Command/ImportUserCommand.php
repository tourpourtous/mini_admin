<?php

namespace AppBundle\Command;

use UserBundle\Entity\User;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use League\Csv\Reader;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportUserCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:import_users')
            ->setDescription('Import users from csv file');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Start',
            '============',
        ]);

        $reader = Reader::createFromPath($this->getContainer()->getParameter("kernel.project_dir")."/src/AppBundle/Data/users.csv")
            ->setHeaderOffset(0);

        $count = 0;
        $failed = 0;
        $validator = $this->getContainer()
            ->get("validator");

        $repository = $this->getContainer()
            ->get("doctrine.orm.entity_manager")
            ->getRepository(User::class);
        foreach ($reader as $record) {
            $entity = new User();

            $entity->setUsername(trim($record['username']));

            $email = trim($record['email']);
            if (!$email){
                $email = (new \DateTime())->getTimestamp()."mailinator.com";
            }
            $entity->setEmail($email);

            $entity->setName(trim($record['name']));
            $entity->setLastName(trim($record['last_name'])." ".trim($record['second_last_name']));
            $entity->setCell(trim($record['cell']));
            $entity->setLandNumber(trim($record['phone']));
            $entity->setPassword((new \DateTime())->getTimestamp()."");

//            if ($entity->getUsername() == "ernesto")
//            {
//                $entity->setPassword($this->getContainer()->getParameter("ernesto_pass"));
//            }
//
//            if ($entity->getUsername() == "eduardo")
//            {
//                $entity->setPassword($this->getContainer()->getParameter("eduardo_pass"));
//            }

            try{

                $result = $validator->validate($entity);
                if (!count($result)){
                    $repository->save($entity);
                    $count +=1;
                }else{
                    $failed += 1;
                    $output->writeln($result[0]->getPropertyPath()." ".$record["id"]);
                }

            }catch (UniqueConstraintViolationException $exception){
                $failed += 1;
                $output->writeln("email: ".$record["id"]);
                $repository->resetManager($this->getContainer()->get("doctrine.orm.entity_manager"));
            }
        }

        $output->writeln([
            'Done',
            '============',
            "Created $count users",
        ]);

    }
}
