<?php

namespace AppBundle\Command;

use UserBundle\Entity\User;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use League\Csv\Reader;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MissingUsersCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:get_missing_users')
            ->setDescription('Import users from csv file');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Start',
            '============',
        ]);

        $reader = Reader::createFromPath($this->getContainer()->getParameter("kernel.project_dir")."/src/AppBundle/Data/users.csv")
            ->setHeaderOffset(0);

        $repository = $this->getContainer()
            ->get("doctrine.orm.entity_manager")
            ->getRepository(User::class);
        foreach ($reader as $record) {
            $element = $repository->findOneBy(["username" => $record['username']]);

            if (!$element)
            {
                $_record = implode(" ", [$record["username"], $record["name"], $record["last_name"]]);
                $output->writeln($_record);
            }
        }

        $output->writeln([
            '============',
            'Done'
        ]);

    }
}
