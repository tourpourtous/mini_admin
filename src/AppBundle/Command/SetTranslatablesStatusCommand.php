<?php

namespace AppBundle\Command;


use AppBundle\Repository\VariableRepository;
use ContentBundle\Entity\Destination;
use ContentBundle\Entity\Place;
use ContentBundle\Entity\PlaceCategory;
use InfoBundle\Entity\InfoPageCategory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UserBundle\Entity\User;


class SetTranslatablesStatusCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('app:entity_locale_status')
            ->setDescription('Set the status in tranlatables entities.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Start',
            '============',
        ]);

        VariableRepository::setDefaultLocale("es");
        VariableRepository::setLocales(["es", "en", "fr"]);
        $em = $this->getContainer()
            ->get("doctrine.orm.entity_manager");

        $default_user = $this->getContainer()
            ->get("doctrine.orm.entity_manager")
            ->getRepository(User::class)->findOneBy(["username" => "ernesto"]);

        $repositories = [
            $em->getRepository(Place::class),
            $em->getRepository(Destination::class),
            $em->getRepository(PlaceCategory::class),
            $em->getRepository(InfoPageCategory::class),
        ];

        foreach ($repositories as $repository) {
            foreach ($repository->findAll() as $entity){

                $entity->setUpdatedBy($default_user);
                $repository->save($entity);
            }
        }

        $output->writeln('Done');

    }
}
