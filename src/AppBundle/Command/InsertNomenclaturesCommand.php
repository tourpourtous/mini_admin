<?php

namespace AppBundle\Command;


use AppBundle\Entity\EntityStatus;
use InfoBundle\Entity\InfoPageType;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UserBundle\Entity\User;


class InsertNomenclaturesCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:insert_nomenclatures')
            ->setDescription('Insert nomenclatures records');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Start',
            '============',
        ]);

        $container = $this->getContainer()
            ->get("doctrine.orm.entity_manager");

        // Add nomenclatures to this list to insert the values to database
        $repositories = [
            $container->getRepository(InfoPageType::class)
        ];

        $user_repository = $this->getContainer()
            ->get("doctrine.orm.entity_manager")
            ->getRepository(User::class);
        $default_user = $user_repository->findOneBy(["username" => "ernesto"]);

        foreach ($repositories as $repository)
            foreach ($repository->getFixture() as $entity)
            {
                if (!$repository->findOneBy(["code" => $entity->getCode()]))
                {
                    $entity->setCreatedBy($default_user);
                    $entity->setUpdatedBy($default_user);
                    $repository->save($entity);
                }
            }

        $output->writeln([
            'Done'
        ]);

    }
}
