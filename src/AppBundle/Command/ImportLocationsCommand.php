<?php

namespace AppBundle\Command;

use AppBundle\Entity\City;
use AppBundle\Entity\Country;
use AppBundle\Entity\State;
use UserBundle\Entity\User;
use League\Csv\Reader;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportLocationsCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:import_locations')
            ->setDescription('Import users from csv file');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Start',
            '============',
        ]);

        $repository = $this->getContainer()
            ->get("doctrine.orm.entity_manager")
            ->getRepository(Country::class);

        $user_repository = $this->getContainer()
            ->get("doctrine.orm.entity_manager")
            ->getRepository(User::class);

        $default_user = $user_repository->findOneBy(["username" => "ernesto"]);

        $entity = new Country();
        $entity->setName("Cuba");
        $entity->setCreatedBy($default_user);
        $entity->setUpdatedBy($default_user);
        $entity->setCode("CU");
        $repository->save($entity);

        $country = $repository->findOneBy(["code"=>"CU"]);

        $reader = Reader::createFromPath($this->getContainer()->getParameter("kernel.project_dir")."/src/AppBundle/Data/provincias.csv")
            ->setHeaderOffset(0);

        $state_repository = $this->getContainer()
            ->get("doctrine.orm.entity_manager")
            ->getRepository(State::class);

        foreach ($reader as $record) {
            $entity = new State();

            $entity->setName(trim($record['name']));
            $entity->setCode(trim($record['code']));
            $entity->setCreatedBy($default_user);
            $entity->setUpdatedBy($default_user);
            $entity->setCountry($country);

            $state_repository->save($entity);
        }

        $reader = Reader::createFromPath($this->getContainer()->getParameter("kernel.project_dir")."/src/AppBundle/Data/municipios.csv")
            ->setHeaderOffset(0);

        $city_repository = $this->getContainer()
            ->get("doctrine.orm.entity_manager")
            ->getRepository(City::class);

        foreach ($reader as $record) {
            $entity = new City();

            $entity->setName(trim($record['name']));
            $entity->setCode(trim($record['code']));
            $entity->setCreatedBy($default_user);
            $entity->setUpdatedBy($default_user);

            $entity->setState($state_repository->findOneBy(['code' => trim($record['s_code'])]));

            $city_repository->save($entity);
        }

        $output->writeln([
            'Done'
        ]);

    }
}
