<?php

namespace AppBundle\Command;


use AppBundle\Repository\VariableRepository;
use ContentBundle\Entity\Destination;
use ContentBundle\Entity\Place;
use InfoBundle\Entity\InfoPage;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UserBundle\Entity\User;


class PublishCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:publish')
            ->setDescription('Publish content with the right status');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Start',
            '============',
        ]);

        $container = $this->getContainer()
            ->get("doctrine.orm.entity_manager");
        VariableRepository::setDefaultLocale("es");
        VariableRepository::setLocales(["es", "en", "fr"]);

        // Add nomenclatures to this list to insert the values to database
        $repositories = [
            $container->getRepository(Destination::class),
            $container->getRepository(Place::class),
            $container->getRepository(InfoPage::class)
        ];

        $user_repository = $this->getContainer()
            ->get("doctrine.orm.entity_manager")
            ->getRepository(User::class);
        $default_user = $user_repository->findOneBy(["username" => "ernesto"]);

        foreach ($repositories as $repository)
            foreach ($repository->findAll() as $entity)
            {
                if ($entity->isOk())
                {
                    $entity->setPublished($entity->isOk());
                    $entity->setUpdatedBy($default_user);
                }

                $repository->save($entity);
            }

        $output->writeln([
            'Done'
        ]);

    }
}
