<?php

namespace AppBundle\Command;


use AppBundle\Entity\EntityStatus;
use AppBundle\Entity\Language;
use InfoBundle\Entity\InfoPageType;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UserBundle\Entity\User;


class InsertLanguagesCommand extends ContainerAwareCommand
{
    use TWebCommand;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:insert_languages')
            ->setDescription('Insert languages records');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Start',
            '============',
        ]);

        $container = $this->getContainer()
            ->get("doctrine.orm.entity_manager");

        // Add nomenclatures to this list to insert the values to database
        $repository = $container->getRepository(Language::class);

        $user_repository = $this->getContainer()
            ->get("doctrine.orm.entity_manager")
            ->getRepository(User::class);
        $default_user = $user_repository->findOneBy(["username" => "ernesto"]);

        foreach ($repository->getEntityData() as $data)
            if (!$repository->findOneBy(["es_name" => $data["es"]]))
            {
                $entity = new Language();
                $entity->setEsName($data["es"]);
                $entity->setName($data["name"]);
                $entity->setCreatedBy($default_user);
                $entity->setUpdatedBy($default_user);
                $repository->save($entity);
            }

        $output->writeln([
            'Done'
        ]);

    }
}
