<?php
/**
 * Created by PhpStorm.
 * User: lorna
 * Date: 08/10/2017
 * Time: 4:27 PM
 */

namespace AppBundle\Command;


use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\NullOutput;

trait TWebCommand
{
    public function runCommand($container)
    {
        $this->setContainer($container);
        $this->execute(new StringInput(""), new NullOutput());
    }
}
