<?php
/**
 * User: Ernesto Baez Fildó (ebaezf@gmail.com)
 * Date: 17-09-06
 * Time: 14:24
 */

namespace AppBundle\EventListener;


use AppBundle\Repository\VariableRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;


class BeforeRequestListener
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        VariableRepository::setDefaultLocale($this->container->getParameter("locale"));
        VariableRepository::setLocales($this->container->getParameter("locales"));
    }
}
